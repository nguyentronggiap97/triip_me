-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 29, 2018 lúc 12:07 PM
-- Phiên bản máy phục vụ: 10.1.34-MariaDB
-- Phiên bản PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `project_triip-me`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_commentmeta`
--

CREATE TABLE `triip_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_comments`
--

CREATE TABLE `triip_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `triip_comments`
--

INSERT INTO `triip_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Một người bình luận WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-08-22 04:31:47', '2018-08-22 04:31:47', 'Xin chào, đây là một bình luận\nĐể bắt đầu với quản trị bình luận, chỉnh sửa hoặc xóa bình luận, vui lòng truy cập vào khu vực Bình luận trong trang quản trị.\nAvatar của người bình luận sử dụng <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 63, 'admin', 'nguyentronggiap97@gmail.com', '', '127.0.0.1', '2018-08-24 10:52:36', '2018-08-24 10:52:36', 'wwqewqe', 0, '1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '', 0, 1),
(3, 63, 'admin', 'nguyentronggiap97@gmail.com', '', '', '2018-08-25 09:31:10', '2018-08-25 09:31:10', 'Giáp', 0, '1', '', 'custom-comment-class', 0, 1),
(8, 63, 'admin', 'nguyentronggiap97@gmail.com', '', '', '2018-08-25 09:34:00', '2018-08-25 09:34:00', '1221321 ', 0, '1', '', 'custom-comment-class', 0, 1),
(9, 63, 'admin', 'nguyentronggiap97@gmail.com', '', '', '2018-08-29 06:54:47', '2018-08-29 06:54:47', 'xin chào', 0, '1', '', 'custom-comment-class', 0, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_links`
--

CREATE TABLE `triip_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_options`
--

CREATE TABLE `triip_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `triip_options`
--

INSERT INTO `triip_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost:8080/projects/triip_me', 'yes'),
(2, 'home', 'http://localhost:8080/projects/triip_me', 'yes'),
(3, 'blogname', '', 'yes'),
(4, 'blogdescription', 'Một trang web mới sử dụng WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'nguyentronggiap97@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'j F, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:140:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:12:\"the-tours/?$\";s:29:\"index.php?post_type=the-tours\";s:42:\"the-tours/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=the-tours&feed=$matches[1]\";s:37:\"the-tours/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=the-tours&feed=$matches[1]\";s:29:\"the-tours/page/([0-9]{1,})/?$\";s:47:\"index.php?post_type=the-tours&paged=$matches[1]\";s:40:\"./(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:35:\"./(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:16:\"./(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:28:\"./(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:10:\"./(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:47:\"places/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?places=$matches[1]&feed=$matches[2]\";s:42:\"places/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?places=$matches[1]&feed=$matches[2]\";s:23:\"places/([^/]+)/embed/?$\";s:39:\"index.php?places=$matches[1]&embed=true\";s:35:\"places/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?places=$matches[1]&paged=$matches[2]\";s:17:\"places/([^/]+)/?$\";s:28:\"index.php?places=$matches[1]\";s:55:\"the-categories/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?the-categories=$matches[1]&feed=$matches[2]\";s:50:\"the-categories/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?the-categories=$matches[1]&feed=$matches[2]\";s:31:\"the-categories/([^/]+)/embed/?$\";s:47:\"index.php?the-categories=$matches[1]&embed=true\";s:43:\"the-categories/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?the-categories=$matches[1]&paged=$matches[2]\";s:25:\"the-categories/([^/]+)/?$\";s:36:\"index.php?the-categories=$matches[1]\";s:41:\"wpdiscuz_form/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"wpdiscuz_form/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"wpdiscuz_form/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"wpdiscuz_form/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"wpdiscuz_form/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"wpdiscuz_form/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"wpdiscuz_form/([^/]+)/embed/?$\";s:46:\"index.php?wpdiscuz_form=$matches[1]&embed=true\";s:34:\"wpdiscuz_form/([^/]+)/trackback/?$\";s:40:\"index.php?wpdiscuz_form=$matches[1]&tb=1\";s:42:\"wpdiscuz_form/([^/]+)/page/?([0-9]{1,})/?$\";s:53:\"index.php?wpdiscuz_form=$matches[1]&paged=$matches[2]\";s:49:\"wpdiscuz_form/([^/]+)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?wpdiscuz_form=$matches[1]&cpage=$matches[2]\";s:38:\"wpdiscuz_form/([^/]+)(?:/([0-9]+))?/?$\";s:52:\"index.php?wpdiscuz_form=$matches[1]&page=$matches[2]\";s:30:\"wpdiscuz_form/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"wpdiscuz_form/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"wpdiscuz_form/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"wpdiscuz_form/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"wpdiscuz_form/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"wpdiscuz_form/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:37:\"the-tours/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"the-tours/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"the-tours/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"the-tours/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"the-tours/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"the-tours/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"the-tours/([^/]+)/embed/?$\";s:42:\"index.php?the-tours=$matches[1]&embed=true\";s:30:\"the-tours/([^/]+)/trackback/?$\";s:36:\"index.php?the-tours=$matches[1]&tb=1\";s:50:\"the-tours/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?the-tours=$matches[1]&feed=$matches[2]\";s:45:\"the-tours/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?the-tours=$matches[1]&feed=$matches[2]\";s:38:\"the-tours/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?the-tours=$matches[1]&paged=$matches[2]\";s:45:\"the-tours/([^/]+)/comment-page-([0-9]{1,})/?$\";s:49:\"index.php?the-tours=$matches[1]&cpage=$matches[2]\";s:34:\"the-tours/([^/]+)(?:/([0-9]+))?/?$\";s:48:\"index.php?the-tours=$matches[1]&page=$matches[2]\";s:26:\"the-tours/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"the-tours/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"the-tours/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"the-tours/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"the-tours/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"the-tours/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=21&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:22:\"3F-framework/index.php\";i:1;s:34:\"advanced-custom-fields-pro/acf.php\";i:2;s:35:\"wp-user-avatars/wp-user-avatars.php\";i:3;s:38:\"wpdiscuz-edited/class.WpdiscuzCore.php\";}', 'yes'),
(34, 'category_base', '/.', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'triip me', 'yes'),
(41, 'stylesheet', 'triip me', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '21', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'triip_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:73:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"read_wpdiscuz_form\";b:1;s:19:\"read_wpdiscuz_forms\";b:1;s:18:\"edit_wpdiscuz_form\";b:1;s:19:\"edit_wpdiscuz_forms\";b:1;s:26:\"edit_others_wpdiscuz_forms\";b:1;s:29:\"edit_published_wpdiscuz_forms\";b:1;s:22:\"publish_wpdiscuz_forms\";b:1;s:20:\"delete_wpdiscuz_form\";b:1;s:21:\"delete_wpdiscuz_forms\";b:1;s:28:\"delete_others_wpdiscuz_forms\";b:1;s:29:\"delete_private_wpdiscuz_forms\";b:1;s:31:\"delete_published_wpdiscuz_forms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'vi', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:5:{s:19:\"wp_inactive_widgets\";a:0:{}s:12:\"tw_home_area\";a:0:{}s:14:\"tw_footer_area\";a:0:{}s:10:\"tw_sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:7:{i:1535538708;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1535538832;a:1:{s:28:\"wpdiscuz_gravatars_cache_add\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:27:\"wpdiscuz_cache_add_every_3h\";s:4:\"args\";a:0:{}s:8:\"interval\";i:10800;}}}i:1535560308;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1535603539;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1535617629;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1535625232;a:1:{s:31:\"wpdiscuz_gravatars_cache_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:31:\"wpdiscuz_cache_delete_every_48h\";s:4:\"args\";a:0:{}s:8:\"interval\";i:172800;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1534912461;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(127, 'can_compress_scripts', '1', 'no'),
(143, 'recently_activated', 'a:2:{s:33:\"wp-user-avatar/wp-user-avatar.php\";i:1535011365;s:34:\"advanced-custom-fields-pro/acf.php\";i:1534912461;}', 'yes'),
(149, 'acf_version', '5.6.10', 'yes'),
(151, 'current_theme', 'Triip me', 'yes'),
(152, 'theme_mods_3f-theme-template', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1534921915;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:12:\"tw_home_area\";a:0:{}s:14:\"tw_footer_area\";a:0:{}s:10:\"tw_sidebar\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(153, 'theme_switched', '', 'yes'),
(154, 'widget_twtheme_contact', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(160, 'theme_mods_triip me', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";i:76;}', 'yes'),
(169, 'framework_remove-page-editor', 'a:1:{i:0;s:1:\"2\";}', 'yes'),
(170, 'framework_block-categories', 'a:1:{i:0;s:1:\"1\";}', 'yes'),
(171, 'framework_block-pages', 'a:1:{i:0;s:1:\"2\";}', 'yes'),
(172, 'options_link_facebook', 'https://www.facebook.com/', 'no'),
(173, '_options_link_facebook', 'field_5b7d1e72f3744', 'no'),
(174, 'options_link_twitter', 'https://www.facebook.com/', 'no'),
(175, '_options_link_twitter', 'field_5b7d1e8cf3745', 'no'),
(176, 'options_link_pinterest', 'https://www.facebook.com/', 'no'),
(177, '_options_link_pinterest', 'field_5b7d1eacf3746', 'no'),
(178, 'options_link_instagram', 'https://www.facebook.com/', 'no'),
(179, '_options_link_instagram', 'field_5b7d1ecbf3747', 'no'),
(180, 'options_copyright', '2018 Triip Pte. Ltd. All rights reserved. 2', 'no'),
(181, '_options_copyright', 'field_5b7d1fcde4dc4', 'no'),
(182, 'options_triip_copyright', '2018 Triip Pte. Ltd. All rights reserved. 2', 'no'),
(183, '_options_triip_copyright', 'field_5b7d1fcde4dc4', 'no'),
(200, 'places_children', 'a:2:{i:2;a:3:{i:0;i:3;i:1;i:4;i:2;i:5;}i:6;a:4:{i:0;i:7;i:1;i:8;i:2;i:9;i:3;i:10;}}', 'yes'),
(224, 'widget_wp_user_avatar_profile', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(242, 'wpua_hash_gravatar', 's:74:\"a:1:{s:32:\"b676e08792c230f80d36fbbd74623047\";a:1:{s:10:\"08-23-2018\";b:0;}}\";', 'yes'),
(259, 'the-categories_children', 'a:0:{}', 'yes'),
(260, 'category_children', 'a:0:{}', 'yes'),
(273, 'wc_hash_key', 'f0f559de1fd194c5333b514dceb74374', 'no'),
(274, 'wc_options', 's:3694:\"a:91:{s:14:\"isEnableOnHome\";s:1:\"1\";s:13:\"wc_quick_tags\";i:0;s:27:\"wc_comment_list_update_type\";s:1:\"0\";s:28:\"wc_comment_list_update_timer\";s:2:\"30\";s:21:\"wc_live_update_guests\";s:1:\"1\";s:24:\"wc_comment_editable_time\";s:3:\"900\";s:22:\"wpdiscuz_redirect_page\";s:1:\"0\";s:20:\"wc_is_guest_can_vote\";s:1:\"1\";s:24:\"isLoadOnlyParentComments\";s:1:\"1\";s:19:\"commentListLoadType\";s:1:\"0\";s:27:\"wc_voting_buttons_show_hide\";i:0;s:18:\"votingButtonsStyle\";s:1:\"0\";s:17:\"votingButtonsIcon\";s:16:\"fa-plus|fa-minus\";s:24:\"wc_header_text_show_hide\";i:0;s:18:\"storeCommenterData\";s:2:\"-1\";s:30:\"wc_show_hide_loggedin_username\";s:1:\"1\";s:22:\"hideLoginLinkForGuests\";s:1:\"1\";s:22:\"hideUserSettingsButton\";s:1:\"1\";s:18:\"hideDiscussionStat\";s:1:\"1\";s:17:\"hideRecentAuthors\";s:1:\"1\";s:19:\"displayAntispamNote\";s:1:\"1\";s:26:\"wc_author_titles_show_hide\";i:0;s:22:\"wc_simple_comment_date\";i:0;s:16:\"subscriptionType\";s:1:\"1\";s:27:\"wc_show_hide_reply_checkbox\";s:1:\"1\";s:21:\"isReplyDefaultChecked\";i:0;s:20:\"show_sorting_buttons\";s:1:\"1\";s:18:\"mostVotedByDefault\";i:0;s:41:\"wc_use_postmatic_for_comment_notification\";i:0;s:20:\"wc_comment_text_size\";s:4:\"14px\";s:16:\"wc_form_bg_color\";s:7:\"#F9F9F9\";s:19:\"wc_comment_bg_color\";s:7:\"#FEFEFE\";s:17:\"wc_reply_bg_color\";s:7:\"#F8F8F8\";s:25:\"wc_comment_username_color\";s:7:\"#00B38F\";s:29:\"wc_comment_rating_hover_color\";s:7:\"#FFED85\";s:31:\"wc_comment_rating_inactiv_color\";s:7:\"#DDDDDD\";s:29:\"wc_comment_rating_activ_color\";s:7:\"#FFD700\";s:13:\"wc_blog_roles\";a:7:{s:13:\"administrator\";s:7:\"#00B38F\";s:6:\"editor\";s:7:\"#00B38F\";s:6:\"author\";s:7:\"#00B38F\";s:11:\"contributor\";s:7:\"#00B38F\";s:10:\"subscriber\";s:7:\"#00B38F\";s:11:\"post_author\";s:7:\"#00B38F\";s:5:\"guest\";s:7:\"#00B38F\";}s:20:\"wc_link_button_color\";a:6:{s:20:\"primary_button_color\";s:7:\"#FFFFFF\";s:17:\"primary_button_bg\";s:7:\"#555555\";s:22:\"secondary_button_color\";s:7:\"#777777\";s:23:\"secondary_button_border\";s:7:\"#dddddd\";s:18:\"vote_up_link_color\";s:7:\"#999999\";s:20:\"vote_down_link_color\";s:7:\"#999999\";}s:21:\"wc_input_border_color\";s:7:\"#D9D9D9\";s:30:\"wc_new_loaded_comment_bg_color\";s:7:\"#FFFAD6\";s:18:\"disableFontAwesome\";i:0;s:11:\"disableTips\";i:0;s:18:\"disableProfileURLs\";s:1:\"1\";s:19:\"displayRatingOnPost\";a:1:{i:0;s:5:\"after\";}s:23:\"ratingCssOnNoneSingular\";i:0;s:13:\"wc_custom_css\";s:27:\".comments-area{width:auto;}\";s:25:\"wc_show_plugin_powerid_by\";i:0;s:15:\"wc_is_use_po_mo\";i:0;s:25:\"wc_disable_member_confirm\";s:1:\"1\";s:20:\"disableGuestsConfirm\";s:1:\"1\";s:26:\"wc_comment_text_min_length\";i:1;s:26:\"wc_comment_text_max_length\";s:0:\"\";s:17:\"commentWordsLimit\";i:100;s:19:\"showHideCommentLink\";i:0;s:15:\"hideCommentDate\";i:0;s:21:\"enableImageConversion\";s:1:\"1\";s:17:\"commentLinkFilter\";i:1;s:18:\"isCaptchaInSession\";s:1:\"1\";s:13:\"isUserByEmail\";i:0;s:22:\"commenterNameMinLength\";s:1:\"3\";s:22:\"commenterNameMaxLength\";s:2:\"50\";s:24:\"isNotifyOnCommentApprove\";i:0;s:22:\"isGravatarCacheEnabled\";s:1:\"1\";s:19:\"gravatarCacheMethod\";s:7:\"cronjob\";s:20:\"gravatarCacheTimeout\";s:2:\"10\";s:5:\"theme\";s:11:\"wpd-default\";s:15:\"reverseChildren\";i:0;s:11:\"antispamKey\";s:32:\"l8xiq#9zozine*u*byx!oe7!f$4yccjj\";s:28:\"socialLoginAgreementCheckbox\";s:1:\"1\";s:26:\"socialLoginInSecondaryForm\";i:0;s:13:\"enableFbLogin\";i:0;s:13:\"enableFbShare\";i:0;s:7:\"fbAppID\";s:0:\"\";s:11:\"fbAppSecret\";s:0:\"\";s:18:\"enableTwitterLogin\";i:0;s:18:\"enableTwitterShare\";s:1:\"1\";s:12:\"twitterAppID\";s:0:\"\";s:16:\"twitterAppSecret\";s:0:\"\";s:17:\"enableGoogleLogin\";i:0;s:17:\"enableGoogleShare\";s:1:\"1\";s:11:\"googleAppID\";s:0:\"\";s:13:\"enableOkLogin\";i:0;s:13:\"enableOkShare\";s:1:\"1\";s:7:\"okAppID\";s:0:\"\";s:8:\"okAppKey\";s:0:\"\";s:11:\"okAppSecret\";s:0:\"\";s:13:\"enableVkLogin\";i:0;s:13:\"enableVkShare\";s:1:\"1\";s:7:\"vkAppID\";s:0:\"\";s:11:\"vkAppSecret\";s:0:\"\";}\";', 'yes'),
(275, 'wpdiscuz_form_content_type_rel', 'a:4:{s:4:\"post\";a:1:{s:2:\"vi\";i:65;}s:10:\"attachment\";a:1:{s:2:\"vi\";i:65;}s:4:\"page\";a:1:{s:2:\"vi\";i:65;}s:9:\"the-tours\";a:1:{s:2:\"vi\";i:65;}}', 'yes'),
(276, 'wc_plugin_version', '100.1.2', 'yes'),
(278, 'wc_deactivation_modal_never_show', '1', 'yes'),
(282, 'wpdiscuz_form_post_rel', 'a:0:{}', 'yes'),
(283, '_transient_wpdiscuz_authors_count_63', '1', 'yes'),
(284, '_transient_wpdiscuz_followers_count_63', '0', 'yes'),
(285, '_transient_wpdiscuz_replies_count_63', '0', 'yes'),
(286, '_transient_wpdiscuz_threads_count_63', '3', 'yes'),
(287, '_transient_wpdiscuz_recent_authors_63', 'a:1:{i:0;O:8:\"stdClass\":3:{s:20:\"comment_author_email\";s:27:\"nguyentronggiap97@gmail.com\";s:14:\"comment_author\";s:5:\"admin\";s:7:\"user_id\";s:1:\"1\";}}', 'yes'),
(365, '_site_transient_timeout_theme_roots', '1535525812', 'no'),
(366, '_site_transient_theme_roots', 'a:2:{s:8:\"triip me\";s:7:\"/themes\";s:9:\"triip mex\";s:7:\"/themes\";}', 'no'),
(368, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:62:\"https://downloads.wordpress.org/release/vi/wordpress-4.9.8.zip\";s:6:\"locale\";s:2:\"vi\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:62:\"https://downloads.wordpress.org/release/vi/wordpress-4.9.8.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.8\";s:7:\"version\";s:5:\"4.9.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1535524016;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:4:\"core\";s:4:\"slug\";s:7:\"default\";s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-24 07:41:18\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/vi.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(369, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1535524018;s:7:\"checked\";a:2:{s:8:\"triip me\";s:0:\"\";s:9:\"triip mex\";s:0:\"\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(370, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1535524019;s:7:\"checked\";a:4:{s:22:\"3F-framework/index.php\";s:5:\"1.2.7\";s:34:\"advanced-custom-fields-pro/acf.php\";s:6:\"5.6.10\";s:38:\"wpdiscuz-edited/class.WpdiscuzCore.php\";s:7:\"100.1.2\";s:35:\"wp-user-avatars/wp-user-avatars.php\";s:5:\"1.4.0\";}s:8:\"response\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.7.3\";s:3:\"url\";s:37:\"https://www.advancedcustomfields.com/\";s:6:\"tested\";s:5:\"4.9.9\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:1:{s:7:\"default\";s:66:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:1:{s:35:\"wp-user-avatars/wp-user-avatars.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/wp-user-avatars\";s:4:\"slug\";s:15:\"wp-user-avatars\";s:6:\"plugin\";s:35:\"wp-user-avatars/wp-user-avatars.php\";s:11:\"new_version\";s:5:\"1.4.0\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/wp-user-avatars/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/wp-user-avatars.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-user-avatars/assets/icon-256x256.png?rev=1266776\";s:2:\"1x\";s:68:\"https://ps.w.org/wp-user-avatars/assets/icon-128x128.png?rev=1266776\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/wp-user-avatars/assets/banner-1544x500.png?rev=1862733\";s:2:\"1x\";s:70:\"https://ps.w.org/wp-user-avatars/assets/banner-772x250.png?rev=1862733\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(371, '_site_transient_timeout_browser_11d9a73fb38b2f1c4799e1f5e17c7b14', '1536129042', 'no'),
(372, '_site_transient_browser_11d9a73fb38b2f1c4799e1f5e17c7b14', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"68.0.3440.106\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(378, 'data_contact', 'a:3:{i:1535526262;a:3:{s:4:\"name\";s:22:\"Nguyễn Trọng Giáp\";s:11:\"phonenumber\";s:10:\"0123456789\";s:5:\"email\";s:27:\"nguyentronggiap97@gmail.com\";}i:1535526293;a:3:{s:4:\"name\";s:22:\"Nguyễn Trọng Giáp\";s:11:\"phonenumber\";s:10:\"0123456789\";s:5:\"email\";s:27:\"nguyentronggiap97@gmail.com\";}i:1535536337;a:4:{s:4:\"name\";s:22:\"Nguyễn Trọng Giáp\";s:11:\"phonenumber\";s:10:\"0123456789\";s:7:\"content\";s:9:\"wqewqewq \";s:5:\"email\";s:27:\"nguyentronggiap97@gmail.com\";}}', 'yes'),
(379, 'booked', 'a:1:{i:1535526799;a:8:{s:4:\"name\";s:22:\"Nguyễn Trọng Giáp\";s:5:\"email\";s:27:\"nguyentronggiap97@gmail.com\";s:5:\"field\";s:11:\"01634584008\";s:9:\"start_day\";s:10:\"08.08.2018\";s:7:\"end_day\";s:10:\"22.08.2018\";s:6:\"people\";s:1:\"4\";s:4:\"room\";s:1:\"4\";s:7:\"id_post\";s:2:\"43\";}}', 'yes');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_postmeta`
--

CREATE TABLE `triip_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `triip_postmeta`
--

INSERT INTO `triip_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(8, 8, '_edit_last', '1'),
(9, 8, '_edit_lock', '1535190605:1'),
(10, 14, '_edit_last', '1'),
(11, 14, '_edit_lock', '1535358101:1'),
(12, 21, '_edit_last', '1'),
(13, 21, '_edit_lock', '1535363821:1'),
(14, 21, '_wp_page_template', 'templates/home-page.php'),
(15, 23, '_wp_attached_file', '2018/08/cubemacth.jpg'),
(16, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:21:\"2018/08/cubemacth.jpg\";s:5:\"sizes\";a:1:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:21:\"cubemacth-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(17, 24, '_wp_attached_file', '2018/08/cube-tur.jpg'),
(18, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:20:\"2018/08/cube-tur.jpg\";s:5:\"sizes\";a:1:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:20:\"cube-tur-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(19, 25, '_wp_attached_file', '2018/08/img-col3.jpg'),
(20, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:20:\"2018/08/img-col3.jpg\";s:5:\"sizes\";a:1:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:20:\"img-col3-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(21, 21, 'triip_list_0_icon', '25'),
(22, 21, '_triip_list_0_icon', 'field_5b7d317e7bc24'),
(23, 21, 'triip_list_0_background', '24'),
(24, 21, '_triip_list_0_background', 'field_5b7d31997bc25'),
(25, 21, 'triip_list_0_title', 'PRICE BEAT'),
(26, 21, '_triip_list_0_title', 'field_5b7d31da7bc26'),
(27, 21, 'triip_list_0_list_strength_0_name', 'automagically'),
(28, 21, '_triip_list_0_list_strength_0_name', 'field_5b7d32857bc28'),
(29, 21, 'triip_list_0_list_strength_1_name', 'Best price'),
(30, 21, '_triip_list_0_list_strength_1_name', 'field_5b7d32857bc28'),
(31, 21, 'triip_list_0_list_strength_2_name', 'guaranteed'),
(32, 21, '_triip_list_0_list_strength_2_name', 'field_5b7d32857bc28'),
(33, 21, 'triip_list_0_list_strength', 'a:3:{i:0;s:8:\"strength\";i:1;s:8:\"strength\";i:2;s:8:\"strength\";}'),
(34, 21, '_triip_list_0_list_strength', 'field_5b7d31ee7bc27'),
(35, 21, 'triip_list', 'a:1:{i:0;s:4:\"list\";}'),
(36, 21, '_triip_list', 'field_5b7d2f0f7bc23'),
(37, 26, 'triip_list_0_icon', '25'),
(38, 26, '_triip_list_0_icon', 'field_5b7d317e7bc24'),
(39, 26, 'triip_list_0_background', '24'),
(40, 26, '_triip_list_0_background', 'field_5b7d31997bc25'),
(41, 26, 'triip_list_0_title', 'PRICE BEAT'),
(42, 26, '_triip_list_0_title', 'field_5b7d31da7bc26'),
(43, 26, 'triip_list_0_list_strength_0_name', 'Best price'),
(44, 26, '_triip_list_0_list_strength_0_name', 'field_5b7d32857bc28'),
(45, 26, 'triip_list_0_list_strength_1_name', 'automagically'),
(46, 26, '_triip_list_0_list_strength_1_name', 'field_5b7d32857bc28'),
(47, 26, 'triip_list_0_list_strength_2_name', 'guaranteed'),
(48, 26, '_triip_list_0_list_strength_2_name', 'field_5b7d32857bc28'),
(49, 26, 'triip_list_0_list_strength', 'a:3:{i:0;s:8:\"strength\";i:1;s:8:\"strength\";i:2;s:8:\"strength\";}'),
(50, 26, '_triip_list_0_list_strength', 'field_5b7d31ee7bc27'),
(51, 26, 'triip_list', 'a:1:{i:0;s:4:\"list\";}'),
(52, 26, '_triip_list', 'field_5b7d2f0f7bc23'),
(53, 27, 'triip_list_0_icon', '25'),
(54, 27, '_triip_list_0_icon', 'field_5b7d317e7bc24'),
(55, 27, 'triip_list_0_background', '24'),
(56, 27, '_triip_list_0_background', 'field_5b7d31997bc25'),
(57, 27, 'triip_list_0_title', 'PRICE BEAT'),
(58, 27, '_triip_list_0_title', 'field_5b7d31da7bc26'),
(59, 27, 'triip_list_0_list_strength_0_name', 'automagically'),
(60, 27, '_triip_list_0_list_strength_0_name', 'field_5b7d32857bc28'),
(61, 27, 'triip_list_0_list_strength_1_name', 'Best price'),
(62, 27, '_triip_list_0_list_strength_1_name', 'field_5b7d32857bc28'),
(63, 27, 'triip_list_0_list_strength_2_name', 'guaranteed'),
(64, 27, '_triip_list_0_list_strength_2_name', 'field_5b7d32857bc28'),
(65, 27, 'triip_list_0_list_strength', 'a:3:{i:0;s:8:\"strength\";i:1;s:8:\"strength\";i:2;s:8:\"strength\";}'),
(66, 27, '_triip_list_0_list_strength', 'field_5b7d31ee7bc27'),
(67, 27, 'triip_list', 'a:1:{i:0;s:4:\"list\";}'),
(68, 27, '_triip_list', 'field_5b7d2f0f7bc23'),
(69, 1, '_edit_lock', '1535093781:1'),
(70, 1, '_edit_last', '1'),
(73, 31, '_edit_last', '1'),
(74, 31, '_edit_lock', '1535197351:1'),
(75, 42, '_edit_last', '1'),
(76, 42, '_edit_lock', '1534994919:1'),
(77, 43, '_edit_last', '1'),
(78, 43, '_edit_lock', '1535191595:1'),
(79, 43, 'triip_duration', '6'),
(80, 43, '_triip_duration', 'field_5b7e22bd07349'),
(81, 43, 'triip_departure', '20180825'),
(82, 43, '_triip_departure', 'field_5b7e23400734a'),
(83, 43, 'triip_price_per_participant', '69'),
(84, 43, '_triip_price_per_participant', 'field_5b7e236f0734b'),
(85, 43, 'triip_languages', 'Tiếng anh, tiếng Nga'),
(86, 43, '_triip_languages', 'field_5b7e24360734c'),
(87, 43, 'triip_group_size', '15'),
(88, 43, '_triip_group_size', 'field_5b7e25f8c18fc'),
(89, 43, 'triip_transportation', 'Taxi,\r\nXe ôm'),
(90, 43, '_triip_transportation', 'field_5b7e2455c18fa'),
(91, 43, 'triip_include_tour', 'Vận chuyển bằng xe máy lạnh Hướng dẫn viên nói tiếng Anh địa phương\r\nBữa trưa Việt Nam\r\nTất cả phí tham quan và tham quan như đã đề cập trong chương trình\r\nNước khoáng miễn phí'),
(92, 43, '_triip_include_tour', 'field_5b7e2631c18fd'),
(93, 43, 'triip_exclude_tour', 'Mẹo & chi phí cá nhân\r\nBảo hiểm du lịch / sức khỏe\r\nCác dịch vụ khác không được đề cập trong hành trình'),
(94, 43, '_triip_exclude_tour', 'field_5b7e263dc18fe'),
(95, 43, 'triip_cancellation', ''),
(96, 43, '_triip_cancellation', 'field_5b7e283bab0da'),
(97, 43, 'triip_summary', '<ul class=\"list-inline special-list\">\r\n 	<li>Bangkok: The City of Angels</li>\r\n 	<li>Visit the most popular nightlife areas around Bangkok with the help of an experienced and Thai speaking guide.</li>\r\n 	<li>Avoid the most common tourist traps and scams that one can come across on a night out.</li>\r\n 	<li>Check out entertainment areas such as Nana Plaza, Soi Cowboy and Patpong.</li>\r\n 	<li>Hit the club scene such as Mixx, Levels, Insanity, Swing, Shock 69 and others.</li>\r\n 	<li>Save money by knowing when and how much to pay and being with someone who can help you negotiate.</li>\r\n</ul>'),
(98, 43, '_triip_summary', 'field_5b7e2d6896447'),
(99, 45, '_wp_attached_file', '2018/08/img-col3-1.jpg'),
(100, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:22:\"2018/08/img-col3-1.jpg\";s:5:\"sizes\";a:1:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:22:\"img-col3-1-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(102, 47, '_edit_last', '1'),
(103, 47, '_edit_lock', '1535011561:1'),
(104, 47, '_wp_page_template', 'templates/demo-page.php'),
(105, 51, '_edit_last', '1'),
(106, 51, '_edit_lock', '1535022016:1'),
(107, 51, '_wp_page_template', 'templates/smart-search.php'),
(108, 54, '_wp_attached_file', '2018/08/single-banner.jpg'),
(109, 54, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2700;s:6:\"height\";i:920;s:4:\"file\";s:25:\"2018/08/single-banner.jpg\";s:5:\"sizes\";a:2:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:25:\"single-banner-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tw_large\";a:4:{s:4:\"file\";s:26:\"single-banner-1210x642.jpg\";s:5:\"width\";i:1210;s:6:\"height\";i:642;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(110, 43, '_thumbnail_id', '56'),
(111, 43, 'triip_location', '21 Tran Hung Dao St, Hoan Kiem Dist'),
(112, 43, '_triip_location', 'field_5b7f78392cace'),
(113, 43, 'triip_banner', '54'),
(114, 43, '_triip_banner', 'field_5b7f7ab73052d'),
(115, 56, '_wp_attached_file', '2018/08/item1.jpg'),
(116, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:333;s:4:\"file\";s:17:\"2018/08/item1.jpg\";s:5:\"sizes\";a:1:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:17:\"item1-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(117, 57, '_wp_attached_file', '2018/08/item2.jpg'),
(118, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:333;s:4:\"file\";s:17:\"2018/08/item2.jpg\";s:5:\"sizes\";a:1:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:17:\"item2-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(119, 58, '_wp_attached_file', '2018/08/item8.jpg'),
(120, 58, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:333;s:4:\"file\";s:17:\"2018/08/item8.jpg\";s:5:\"sizes\";a:1:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:17:\"item8-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(121, 59, '_wp_attached_file', '2018/08/item9.jpg'),
(122, 59, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:333;s:4:\"file\";s:17:\"2018/08/item9.jpg\";s:5:\"sizes\";a:1:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:17:\"item9-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(123, 60, '_edit_last', '1'),
(124, 60, '_edit_lock', '1535358155:1'),
(128, 63, '_edit_last', '1'),
(129, 63, '_edit_lock', '1535191142:1'),
(132, 63, 'triip_summary', '<ul class=\"list-inline special-list\">\r\n 	<li>Bangkok: The City of Angels</li>\r\n 	<li>Visit the most popular nightlife areas around Bangkok with the help of an experienced and Thai speaking guide.</li>\r\n 	<li>Avoid the most common tourist traps and scams that one can come across on a night out.</li>\r\n 	<li>Check out entertainment areas such as Nana Plaza, Soi Cowboy and Patpong.</li>\r\n 	<li>Hit the club scene such as Mixx, Levels, Insanity, Swing, Shock 69 and others.</li>\r\n 	<li>Save money by knowing when and how much to pay and being with someone who can help you negotiate.</li>\r\n</ul>'),
(133, 63, '_triip_summary', 'field_5b7e2d6896447'),
(134, 63, 'triip_banner', '54'),
(135, 63, '_triip_banner', 'field_5b7f7ab73052d'),
(136, 63, 'triip_duration', '6'),
(137, 63, '_triip_duration', 'field_5b7e22bd07349'),
(138, 63, 'triip_departure', '20180825'),
(139, 63, '_triip_departure', 'field_5b7e23400734a'),
(140, 63, 'triip_price_per_participant', '60'),
(141, 63, '_triip_price_per_participant', 'field_5b7e236f0734b'),
(142, 63, 'triip_languages', 'Tiếng anh, tiếng Nga'),
(143, 63, '_triip_languages', 'field_5b7e24360734c'),
(144, 63, 'triip_group_size', '10'),
(145, 63, '_triip_group_size', 'field_5b7e25f8c18fc'),
(146, 63, 'triip_transportation', 'Taxi,\r\nXe ôm'),
(147, 63, '_triip_transportation', 'field_5b7e2455c18fa'),
(148, 63, 'triip_include_tour', 'Vận chuyển bằng xe máy lạnh Hướng dẫn viên nói tiếng Anh địa phương\r\nBữa trưa Việt Nam\r\nTất cả phí tham quan và tham quan như đã đề cập trong chương trình\r\nNước khoáng miễn phí'),
(149, 63, '_triip_include_tour', 'field_5b7e2631c18fd'),
(150, 63, 'triip_exclude_tour', 'Mẹo & chi phí cá nhân\r\nBảo hiểm du lịch / sức khỏe\r\nCác dịch vụ khác không được đề cập trong hành trình'),
(151, 63, '_triip_exclude_tour', 'field_5b7e263dc18fe'),
(152, 63, 'triip_cancellation', ''),
(153, 63, '_triip_cancellation', 'field_5b7e283bab0da'),
(154, 63, 'triip_location', '21 Tran Duy Hung, Thanh Xuan, Ha Noi'),
(155, 63, '_triip_location', 'field_5b7f78392cace'),
(158, 63, '_thumbnail_id', '57'),
(159, 63, 'triip_ranker', '6'),
(160, 63, '_triip_ranker', 'field_5b7fb32a250d4'),
(161, 54, '_edit_lock', '1535096747:1'),
(162, 43, 'triip_ranker', '3'),
(163, 43, '_triip_ranker', 'field_5b7fb32a250d4'),
(206, 43, 'count_view', '20'),
(207, 63, 'count_view', '29'),
(208, 65, 'wpd_form_custom_css', ''),
(209, 65, 'wpdiscuz_form_general_options', 'a:8:{s:4:\"lang\";s:2:\"vi\";s:20:\"roles_cannot_comment\";a:0:{}s:17:\"guest_can_comment\";i:1;s:21:\"show_subscription_bar\";i:1;s:11:\"header_text\";s:13:\"Leave a Reply\";s:24:\"wpdiscuz_form_post_types\";a:4:{s:4:\"post\";s:4:\"post\";s:4:\"page\";s:4:\"page\";s:10:\"attachment\";s:10:\"attachment\";s:9:\"the-tours\";s:9:\"the-tours\";}s:6:\"postid\";s:0:\"\";s:12:\"postidsArray\";a:0:{}}'),
(210, 65, 'wpdiscuz_form_structure', 'a:1:{s:19:\"wpd_form_row_wrap_0\";a:4:{s:11:\"column_type\";s:3:\"two\";s:9:\"row_order\";i:0;s:4:\"left\";a:3:{s:7:\"wc_name\";a:7:{s:4:\"name\";s:4:\"Name\";s:4:\"desc\";s:0:\"\";s:4:\"icon\";s:11:\"fas fa-user\";s:8:\"required\";i:1;s:4:\"type\";s:35:\"wpdFormAttr\\Field\\DefaultField\\Name\";s:18:\"is_show_on_comment\";i:0;s:13:\"is_show_sform\";i:0;}s:8:\"wc_email\";a:7:{s:4:\"name\";s:5:\"Email\";s:4:\"desc\";s:0:\"\";s:4:\"icon\";s:9:\"fas fa-at\";s:8:\"required\";i:1;s:4:\"type\";s:36:\"wpdFormAttr\\Field\\DefaultField\\Email\";s:18:\"is_show_on_comment\";i:0;s:13:\"is_show_sform\";i:0;}s:10:\"wc_website\";a:5:{s:4:\"name\";s:7:\"Website\";s:4:\"desc\";s:0:\"\";s:4:\"icon\";s:11:\"fas fa-link\";s:6:\"enable\";i:1;s:4:\"type\";s:38:\"wpdFormAttr\\Field\\DefaultField\\Website\";}}s:5:\"right\";a:2:{s:10:\"wc_captcha\";a:5:{s:4:\"name\";s:4:\"Code\";s:4:\"desc\";s:0:\"\";s:15:\"show_for_guests\";s:1:\"0\";s:14:\"show_for_users\";s:1:\"0\";s:4:\"type\";s:38:\"wpdFormAttr\\Field\\DefaultField\\Captcha\";}s:6:\"submit\";a:2:{s:4:\"name\";s:12:\"Post Comment\";s:4:\"type\";s:37:\"wpdFormAttr\\Field\\DefaultField\\Submit\";}}}}'),
(211, 65, 'wpdiscuz_form_fields', 'a:5:{s:7:\"wc_name\";a:7:{s:4:\"name\";s:4:\"Name\";s:4:\"desc\";s:0:\"\";s:4:\"icon\";s:11:\"fas fa-user\";s:8:\"required\";i:1;s:4:\"type\";s:35:\"wpdFormAttr\\Field\\DefaultField\\Name\";s:18:\"is_show_on_comment\";i:0;s:13:\"is_show_sform\";i:0;}s:8:\"wc_email\";a:7:{s:4:\"name\";s:5:\"Email\";s:4:\"desc\";s:0:\"\";s:4:\"icon\";s:9:\"fas fa-at\";s:8:\"required\";i:1;s:4:\"type\";s:36:\"wpdFormAttr\\Field\\DefaultField\\Email\";s:18:\"is_show_on_comment\";i:0;s:13:\"is_show_sform\";i:0;}s:10:\"wc_website\";a:5:{s:4:\"name\";s:7:\"Website\";s:4:\"desc\";s:0:\"\";s:4:\"icon\";s:11:\"fas fa-link\";s:6:\"enable\";i:1;s:4:\"type\";s:38:\"wpdFormAttr\\Field\\DefaultField\\Website\";}s:10:\"wc_captcha\";a:5:{s:4:\"name\";s:4:\"Code\";s:4:\"desc\";s:0:\"\";s:15:\"show_for_guests\";s:1:\"0\";s:14:\"show_for_users\";s:1:\"0\";s:4:\"type\";s:38:\"wpdFormAttr\\Field\\DefaultField\\Captcha\";}s:6:\"submit\";a:2:{s:4:\"name\";s:12:\"Post Comment\";s:4:\"type\";s:37:\"wpdFormAttr\\Field\\DefaultField\\Submit\";}}'),
(212, 63, 'count_view', '29'),
(213, 65, '_edit_lock', '1535108440:1'),
(214, 65, '_edit_last', '1'),
(215, 43, 'count_view', '20'),
(216, 63, 'count_view', '29'),
(217, 43, 'count_view', '20'),
(218, 43, 'ranker', 'a:1:{i:1;s:1:\"5\";}'),
(219, 43, 'ranker', 'a:1:{i:1;s:1:\"5\";}'),
(220, 63, 'count_view', '29'),
(221, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(222, 63, 'ranker', 'a:1:{i:0;s:1:\"3\";}'),
(223, 63, 'ranker', 'a:1:{i:0;s:1:\"3\";}'),
(224, 63, 'ranker', 'a:1:{i:0;s:1:\"3\";}'),
(225, 63, 'ranker', 'a:1:{i:0;s:1:\"3\";}'),
(226, 63, 'ranker', 'a:1:{i:0;s:1:\"3\";}'),
(227, 63, 'ranker', 'a:1:{i:0;s:1:\"3\";}'),
(228, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(229, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(230, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(231, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(232, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(233, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(234, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(235, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(236, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(237, 43, 'ranker', 'a:1:{i:1;s:1:\"5\";}'),
(238, 43, 'ranker', 'a:1:{i:1;s:1:\"5\";}'),
(239, 43, 'ranker', 'a:1:{i:1;s:1:\"5\";}'),
(240, 43, 'ranker', 'a:1:{i:1;s:1:\"5\";}'),
(241, 43, 'ranker', 'a:1:{i:1;s:1:\"5\";}'),
(242, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(243, 63, 'ranker', 'a:1:{i:1;s:1:\"3\";}'),
(244, 63, 'triip_review_score', '9.3'),
(245, 63, '_triip_review_score', 'field_5b7fb32a250d4'),
(246, 43, 'triip_review_score', '8.3'),
(247, 43, '_triip_review_score', 'field_5b7fb32a250d4'),
(248, 66, '_edit_lock', '1535360165:1'),
(249, 66, '_edit_last', '1'),
(250, 68, '_wp_attached_file', '2018/08/banner.jpg'),
(251, 68, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:900;s:4:\"file\";s:18:\"2018/08/banner.jpg\";s:5:\"sizes\";a:2:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tw_large\";a:4:{s:4:\"file\";s:19:\"banner-1210x642.jpg\";s:5:\"width\";i:1210;s:6:\"height\";i:642;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(252, 21, 'triip_banner', '68'),
(253, 21, '_triip_banner', 'field_5b83604a35145'),
(254, 69, 'triip_list_0_icon', '25'),
(255, 69, '_triip_list_0_icon', 'field_5b7d317e7bc24'),
(256, 69, 'triip_list_0_background', '24'),
(257, 69, '_triip_list_0_background', 'field_5b7d31997bc25'),
(258, 69, 'triip_list_0_title', 'PRICE BEAT'),
(259, 69, '_triip_list_0_title', 'field_5b7d31da7bc26'),
(260, 69, 'triip_list_0_list_strength_0_name', 'automagically'),
(261, 69, '_triip_list_0_list_strength_0_name', 'field_5b7d32857bc28'),
(262, 69, 'triip_list_0_list_strength_1_name', 'Best price'),
(263, 69, '_triip_list_0_list_strength_1_name', 'field_5b7d32857bc28'),
(264, 69, 'triip_list_0_list_strength_2_name', 'guaranteed'),
(265, 69, '_triip_list_0_list_strength_2_name', 'field_5b7d32857bc28'),
(266, 69, 'triip_list_0_list_strength', 'a:3:{i:0;s:8:\"strength\";i:1;s:8:\"strength\";i:2;s:8:\"strength\";}'),
(267, 69, '_triip_list_0_list_strength', 'field_5b7d31ee7bc27'),
(268, 69, 'triip_list', 'a:1:{i:0;s:4:\"list\";}'),
(269, 69, '_triip_list', 'field_5b7d2f0f7bc23'),
(270, 69, 'triip_banner', 'a:1:{i:0;s:2:\"68\";}'),
(271, 69, '_triip_banner', 'field_5b83604a35145'),
(272, 70, 'triip_list_0_icon', '25'),
(273, 70, '_triip_list_0_icon', 'field_5b7d317e7bc24'),
(274, 70, 'triip_list_0_background', '24'),
(275, 70, '_triip_list_0_background', 'field_5b7d31997bc25'),
(276, 70, 'triip_list_0_title', 'PRICE BEAT'),
(277, 70, '_triip_list_0_title', 'field_5b7d31da7bc26'),
(278, 70, 'triip_list_0_list_strength_0_name', 'automagically'),
(279, 70, '_triip_list_0_list_strength_0_name', 'field_5b7d32857bc28'),
(280, 70, 'triip_list_0_list_strength_1_name', 'Best price'),
(281, 70, '_triip_list_0_list_strength_1_name', 'field_5b7d32857bc28'),
(282, 70, 'triip_list_0_list_strength_2_name', 'guaranteed'),
(283, 70, '_triip_list_0_list_strength_2_name', 'field_5b7d32857bc28'),
(284, 70, 'triip_list_0_list_strength', 'a:3:{i:0;s:8:\"strength\";i:1;s:8:\"strength\";i:2;s:8:\"strength\";}'),
(285, 70, '_triip_list_0_list_strength', 'field_5b7d31ee7bc27'),
(286, 70, 'triip_list', 'a:1:{i:0;s:4:\"list\";}'),
(287, 70, '_triip_list', 'field_5b7d2f0f7bc23'),
(288, 70, 'triip_banner', '68'),
(289, 70, '_triip_banner', 'field_5b83604a35145'),
(290, 21, 'triip_home_title', 'Book a stay & First content.\r\nby locals seamlessly'),
(291, 21, '_triip_home_title', 'field_5b83b5eae0bb3'),
(292, 72, 'triip_list_0_icon', '25'),
(293, 72, '_triip_list_0_icon', 'field_5b7d317e7bc24'),
(294, 72, 'triip_list_0_background', '24'),
(295, 72, '_triip_list_0_background', 'field_5b7d31997bc25'),
(296, 72, 'triip_list_0_title', 'PRICE BEAT'),
(297, 72, '_triip_list_0_title', 'field_5b7d31da7bc26'),
(298, 72, 'triip_list_0_list_strength_0_name', 'automagically'),
(299, 72, '_triip_list_0_list_strength_0_name', 'field_5b7d32857bc28'),
(300, 72, 'triip_list_0_list_strength_1_name', 'Best price'),
(301, 72, '_triip_list_0_list_strength_1_name', 'field_5b7d32857bc28'),
(302, 72, 'triip_list_0_list_strength_2_name', 'guaranteed'),
(303, 72, '_triip_list_0_list_strength_2_name', 'field_5b7d32857bc28'),
(304, 72, 'triip_list_0_list_strength', 'a:3:{i:0;s:8:\"strength\";i:1;s:8:\"strength\";i:2;s:8:\"strength\";}'),
(305, 72, '_triip_list_0_list_strength', 'field_5b7d31ee7bc27'),
(306, 72, 'triip_list', 'a:1:{i:0;s:4:\"list\";}'),
(307, 72, '_triip_list', 'field_5b7d2f0f7bc23'),
(308, 72, 'triip_banner', '68'),
(309, 72, '_triip_banner', 'field_5b83604a35145'),
(310, 72, 'triip_home_title', 'Book a stay & First content.\r\nby locals seamlessly'),
(311, 72, '_triip_home_title', 'field_5b83b5eae0bb3'),
(312, 73, '_edit_lock', '1535361872:1'),
(313, 74, '_edit_lock', '1535363687:1'),
(314, 75, '_wp_attached_file', '2018/08/40221133_301201817353120_7401667735635099648_n.jpg'),
(315, 75, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1900;s:6:\"height\";i:1381;s:4:\"file\";s:58:\"2018/08/40221133_301201817353120_7401667735635099648_n.jpg\";s:5:\"sizes\";a:2:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:58:\"40221133_301201817353120_7401667735635099648_n-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tw_large\";a:4:{s:4:\"file\";s:59:\"40221133_301201817353120_7401667735635099648_n-1210x642.jpg\";s:5:\"width\";i:1210;s:6:\"height\";i:642;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(316, 76, '_wp_attached_file', '2018/08/cropped-40221133_301201817353120_7401667735635099648_n.jpg'),
(317, 76, '_wp_attachment_context', 'custom-logo'),
(318, 76, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1724;s:6:\"height\";i:1381;s:4:\"file\";s:66:\"2018/08/cropped-40221133_301201817353120_7401667735635099648_n.jpg\";s:5:\"sizes\";a:2:{s:12:\"tw_thumbnail\";a:4:{s:4:\"file\";s:66:\"cropped-40221133_301201817353120_7401667735635099648_n-360x230.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:8:\"tw_large\";a:4:{s:4:\"file\";s:67:\"cropped-40221133_301201817353120_7401667735635099648_n-1210x642.jpg\";s:5:\"width\";i:1210;s:6:\"height\";i:642;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(319, 77, '_wp_trash_meta_status', 'publish'),
(320, 77, '_wp_trash_meta_time', '1535524338'),
(321, 78, '_edit_lock', '1535526491:1'),
(322, 78, '_edit_last', '1'),
(323, 78, '_wp_page_template', 'templates/contact-page.php'),
(324, 63, 'count_view', '1'),
(325, 43, 'count_view', '1');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_posts`
--

CREATE TABLE `triip_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `triip_posts`
--

INSERT INTO `triip_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-08-22 04:31:47', '2018-08-22 04:31:47', 'Chúc mừng đến với WordPress. Đây là bài viết đầu tiên của bạn. Hãy chỉnh sửa hay xóa bài viết này, và bắt đầu viết blog!a\r\n<ul>\r\n 	<li>adsadsad</li>\r\n 	<li>asdsadsad</li>\r\n 	<li>asdsa</li>\r\n</ul>', 'Chào tất cả mọi người!', '', 'publish', 'open', 'open', '', 'chao-moi-nguoi', '', '', '2018-08-23 02:36:14', '2018-08-23 02:36:14', '', 0, 'http://localhost:8080/projects/triip_me/?p=1', 0, 'post', '', 1),
(2, 1, '2018-08-22 04:31:47', '2018-08-22 04:31:47', 'Đây là một trang mẫu. Nó khác với một bài blog bởi vì nó sẽ là một trang tĩnh và sẽ được thêm vào thanh menu của trang web của bạn (trong hầu hết theme). Mọi người thường bắt đầu bằng một trang Giới thiệu để giới thiệu bản thân đến người dùng tiềm năng. Bạn có thể viết như sau:\n\n<blockquote>Xin chào! Tôi là người giao thư bằng xe đạp vào ban ngày, một diễn viên đầy tham vọng vào ban đêm, và đây là trang web của tôi. Tôi sống ở Los Angeles, có một chú cho tuyệt vời tên là Jack, và tôi thích uống cocktail.</blockquote>\n\n...hay như thế này:\n\n<blockquote>Công ty XYZ Doohickey được thành lập vào năm 1971, và đã cung cấp đồ dùng chất lượng cho công chúng kể từ đó. Nằm ở thành phố Gotham, XYZ tạo việc làm cho hơn 2.000 người và làm tất cả những điều tuyệt vời cho cộng đồng Gotham.</blockquote>\n\nLà người dùng WordPress mới, bạn nên truy cập <a href=\"http://localhost:8080/projects/triip_me/wp-admin/\">trang quản trị</a> để xóa trang này và tạo các trang mới cho nội dung của bạn. Chúc vui vẻ!', 'Trang Mẫu', '', 'publish', 'closed', 'open', '', 'Trang mẫu', '', '', '2018-08-22 04:31:47', '2018-08-22 04:31:47', '', 0, 'http://localhost:8080/projects/triip_me/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-08-22 04:31:47', '2018-08-22 04:31:47', '<h2>Chúng tôi là ai</h2><p>Địa chỉ website là: http://localhost:8080/projects/triip_me.</p><h2>Thông tin cá nhân nào bị thu thập và tại sao thu thập</h2><h3>Bình luận</h3><p>Khi khách truy cập để lại bình luận trên trang web, chúng tôi thu thập dữ liệu được hiển thị trong biểu mẫu bình luận và cũng là địa chỉ IP của người truy cập và chuỗi user agent của người dùng trình duyệt để giúp phát hiện spam</p><p>Một chuỗi ẩn danh được tạo từ địa chỉ email của bạn (còn được gọi là hash) có thể được cung cấp cho dịch vụ Gravatar để xem bạn có đang sử dụng nó hay không. Chính sách bảo mật của dịch vụ Gravatar có tại đây: https://automattic.com/privacy/. Sau khi chấp nhận bình luận của bạn, ảnh tiểu sử của bạn được hiển thị công khai trong ngữ cảnh bình luận của bạn.</p><h3>Thư viện</h3><p>Nếu bạn tải hình ảnh lên trang web, bạn nên tránh tải lên hình ảnh có dữ liệu vị trí được nhúng (EXIF GPS) đi kèm. Khách truy cập vào trang web có thể tải xuống và giải nén bất kỳ dữ liệu vị trí nào từ hình ảnh trên trang web.</p><h3>Thông tin liên hệ</h3><h3>Cookies</h3><p>Nếu bạn viết bình luận trong website, bạn có thể cung cấp cần nhập tên, email địa chỉ website trong cookie. Các thông tin này nhằm giúp bạn không cần nhập thông tin nhiều lần khi viết bình luận khác. Cookie này sẽ được lưu giữ trong một năm.</p><p>Nếu bạn có tài khoản và đăng nhập và website, chúng tôi sẽ thiết lập một cookie tạm thời để xác định nếu trình duyệt cho phép sử dụng cookie. Cookie này không bao gồm thông tin cá nhân và sẽ được gỡ bỏ khi bạn đóng trình duyệt.</p><p>Khi bạn đăng nhập, chúng tôi sẽ thiết lập một vài cookie để lưu thông tin đăng nhập và lựa chọn hiển thị. Thông tin đăng nhập gần nhất lưu trong hai ngày, và lựa chọn hiển thị gần nhất lưu trong một năm. Nếu bạn chọn &quot;Nhớ tôi&quot;, thông tin đăng nhập sẽ được lưu trong hai tuần. Nếu bạn thoát tài khoản, thông tin cookie đăng nhập sẽ bị xoá.</p><p>Nếu bạn sửa hoặc công bố bài viết, một bản cookie bổ sung sẽ được lưu trong trình duyệt. Cookie này không chứa thông tin cá nhân và chỉ đơn giản bao gồm ID của bài viết bạn đã sửa. Nó tự động hết hạn sau 1 ngày.</p><h3>Nội dung nhúng từ website khác</h3><p>Các bài viết trên trang web này có thể bao gồm nội dung được nhúng (ví dụ: video, hình ảnh, bài viết, v.v.). Nội dung được nhúng từ các trang web khác hoạt động theo cùng một cách chính xác như khi khách truy cập đã truy cập trang web khác.</p><p>Những website này có thể thu thập dữ liệu về bạn, sử dụng cookie, nhúng các trình theo dõi của bên thứ ba và giám sát tương tác của bạn với nội dung được nhúng đó, bao gồm theo dõi tương tác của bạn với nội dung được nhúng nếu bạn có tài khoản và đã đăng nhập vào trang web đó.</p><h3>Phân tích</h3><h2>Chúng tôi chia sẻ dữ liệu của bạn với ai</h2><h2>Dữ liệu của bạn tồn tại bao lâu</h2><p>Nếu bạn để lại bình luận, bình luận và siêu dữ liệu của nó sẽ được giữ lại vô thời hạn. Điều này là để chúng tôi có thể tự động nhận ra và chấp nhận bất kỳ bình luận nào thay vì giữ chúng trong khu vực đợi kiểm duyệt.</p><p>Đối với người dùng đăng ký trên trang web của chúng tôi (nếu có), chúng tôi cũng lưu trữ thông tin cá nhân mà họ cung cấp trong hồ sơ người dùng của họ. Tất cả người dùng có thể xem, chỉnh sửa hoặc xóa thông tin cá nhân của họ bất kỳ lúc nào (ngoại trừ họ không thể thay đổi tên người dùng của họ). Quản trị viên trang web cũng có thể xem và chỉnh sửa thông tin đó.</p><h2>Các quyền nào của bạn với dữ liệu của mình</h2><p>Nếu bạn có tài khoản trên trang web này hoặc đã để lại nhận xét, bạn có thể yêu cầu nhận tệp xuất dữ liệu cá nhân mà chúng tôi lưu giữ về bạn, bao gồm mọi dữ liệu bạn đã cung cấp cho chúng tôi. Bạn cũng có thể yêu cầu chúng tôi xóa mọi dữ liệu cá nhân mà chúng tôi lưu giữ về bạn. Điều này không bao gồm bất kỳ dữ liệu nào chúng tôi có nghĩa vụ giữ cho các mục đích hành chính, pháp lý hoặc bảo mật.</p><h2>Các dữ liệu của bạn được gửi tới đâu</h2><p>Các bình luận của khách (không phải là thành viên) có thể được kiểm tra thông qua dịch vụ tự động phát hiện spam.</p><h2>Thông tin liên hệ của bạn</h2><h2>Thông tin bổ sung</h2><h3>Cách chúng tôi bảo vệ dữ liệu của bạn</h3><h3>Các quá trình tiết lộ dữ liệu mà chúng tôi thực hiện</h3><h3>Những bên thứ ba chúng tôi nhận dữ liệu từ đó</h3><h3>Việc quyết định và/hoặc thu thập thông tin tự động mà chúng tôi áp dụng với dữ liệu người dùng</h3><h3>Các yêu cầu công bố thông tin được quản lý</h3>', 'Chính sách bảo mật', '', 'draft', 'closed', 'open', '', 'chinh-sach-bao-mat', '', '', '2018-08-22 04:31:47', '2018-08-22 04:31:47', '', 0, 'http://localhost:8080/projects/triip_me/?page_id=3', 0, 'page', '', 0),
(8, 1, '2018-08-22 08:30:07', '2018-08-22 08:30:07', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:21:\"acf-options-tuy-chinh\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Thông tin liên hệ', 'thong-tin-lien-he', 'publish', 'closed', 'closed', '', 'group_5b7d1e5dd332a', '', '', '2018-08-22 09:01:12', '2018-08-22 09:01:12', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=acf-field-group&#038;p=8', 0, 'acf-field-group', '', 0),
(9, 1, '2018-08-22 08:30:07', '2018-08-22 08:30:07', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"24\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Địa chỉ facebook', 'link_facebook', 'publish', 'closed', 'closed', '', 'field_5b7d1e72f3744', '', '', '2018-08-22 08:32:54', '2018-08-22 08:32:54', '', 8, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=9', 0, 'acf-field', '', 0),
(10, 1, '2018-08-22 08:30:07', '2018-08-22 08:30:07', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"24\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Địa chỉ Twitter', 'link_twitter', 'publish', 'closed', 'closed', '', 'field_5b7d1e8cf3745', '', '', '2018-08-22 08:32:54', '2018-08-22 08:32:54', '', 8, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=10', 1, 'acf-field', '', 0),
(11, 1, '2018-08-22 08:30:07', '2018-08-22 08:30:07', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"24\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Địa chỉ Pinterest', 'link_pinterest', 'publish', 'closed', 'closed', '', 'field_5b7d1eacf3746', '', '', '2018-08-22 08:30:55', '2018-08-22 08:30:55', '', 8, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=11', 2, 'acf-field', '', 0),
(12, 1, '2018-08-22 08:30:07', '2018-08-22 08:30:07', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"24\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Địa chỉ Instagram', 'link_instagram', 'publish', 'closed', 'closed', '', 'field_5b7d1ecbf3747', '', '', '2018-08-22 08:30:55', '2018-08-22 08:30:55', '', 8, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=12', 3, 'acf-field', '', 0),
(13, 1, '2018-08-22 08:33:24', '2018-08-22 08:33:24', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Copyright', 'triip_copyright', 'publish', 'closed', 'closed', '', 'field_5b7d1fcde4dc4', '', '', '2018-08-22 09:01:12', '2018-08-22 09:01:12', '', 8, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=13', 4, 'acf-field', '', 0),
(14, 1, '2018-08-22 09:53:57', '2018-08-22 09:53:57', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"21\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'WHAT MAKES US BETTER?', 'what-makes-us-better', 'publish', 'closed', 'closed', '', 'group_5b7d2f09cad9d', '', '', '2018-08-27 08:23:30', '2018-08-27 08:23:30', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=acf-field-group&#038;p=14', 0, 'acf-field-group', '', 0),
(15, 1, '2018-08-22 09:53:57', '2018-08-22 09:53:57', 'a:9:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"layouts\";a:1:{s:13:\"5b7d2f4a641a8\";a:6:{s:3:\"key\";s:13:\"5b7d2f4a641a8\";s:5:\"label\";s:4:\"List\";s:4:\"name\";s:4:\"list\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:7:\"Add Row\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'List', 'triip_list', 'publish', 'closed', 'closed', '', 'field_5b7d2f0f7bc23', '', '', '2018-08-22 09:53:57', '2018-08-22 09:53:57', '', 14, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&p=15', 0, 'acf-field', '', 0),
(16, 1, '2018-08-22 09:53:57', '2018-08-22 09:53:57', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"5b7d2f4a641a8\";s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Icon', 'icon', 'publish', 'closed', 'closed', '', 'field_5b7d317e7bc24', '', '', '2018-08-27 08:23:30', '2018-08-27 08:23:30', '', 15, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=16', 0, 'acf-field', '', 0),
(17, 1, '2018-08-22 09:53:57', '2018-08-22 09:53:57', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"40\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"5b7d2f4a641a8\";s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'background', 'background', 'publish', 'closed', 'closed', '', 'field_5b7d31997bc25', '', '', '2018-08-27 08:23:30', '2018-08-27 08:23:30', '', 15, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=17', 1, 'acf-field', '', 0),
(18, 1, '2018-08-22 09:53:57', '2018-08-22 09:53:57', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"40\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"5b7d2f4a641a8\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_5b7d31da7bc26', '', '', '2018-08-27 08:23:30', '2018-08-27 08:23:30', '', 15, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=18', 2, 'acf-field', '', 0),
(19, 1, '2018-08-22 09:53:57', '2018-08-22 09:53:57', 'a:10:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"5b7d2f4a641a8\";s:7:\"layouts\";a:1:{s:13:\"5b7d31f818ecd\";a:6:{s:3:\"key\";s:13:\"5b7d31f818ecd\";s:5:\"label\";s:9:\"Strengths\";s:4:\"name\";s:8:\"strength\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:7:\"Add Row\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'List Strengths', 'list_strength', 'publish', 'closed', 'closed', '', 'field_5b7d31ee7bc27', '', '', '2018-08-22 09:53:57', '2018-08-22 09:53:57', '', 15, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&p=19', 3, 'acf-field', '', 0),
(20, 1, '2018-08-22 09:53:57', '2018-08-22 09:53:57', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"5b7d31f818ecd\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Strength', 'name', 'publish', 'closed', 'closed', '', 'field_5b7d32857bc28', '', '', '2018-08-22 09:53:57', '2018-08-22 09:53:57', '', 19, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&p=20', 0, 'acf-field', '', 0),
(21, 1, '2018-08-22 09:54:54', '2018-08-22 09:54:54', '', 'Trang chủ', '', 'publish', 'closed', 'closed', '', 'trang-chu', '', '', '2018-08-27 08:35:01', '2018-08-27 08:35:01', '', 0, 'http://localhost:8080/projects/triip_me/?page_id=21', 0, 'page', '', 0),
(22, 1, '2018-08-22 09:54:54', '2018-08-22 09:54:54', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2018-08-22 09:54:54', '2018-08-22 09:54:54', '', 21, 'http://localhost:8080/projects/triip_me/2018/08/22/21-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2018-08-22 09:57:50', '2018-08-22 09:57:50', '', 'cubemacth', '', 'inherit', 'open', 'closed', '', 'cubemacth', '', '', '2018-08-22 09:57:50', '2018-08-22 09:57:50', '', 21, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/cubemacth.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2018-08-22 09:57:51', '2018-08-22 09:57:51', '', 'cube-tur', '', 'inherit', 'open', 'closed', '', 'cube-tur', '', '', '2018-08-22 09:57:51', '2018-08-22 09:57:51', '', 21, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/cube-tur.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2018-08-22 09:57:53', '2018-08-22 09:57:53', '', 'img-col3', '', 'inherit', 'open', 'closed', '', 'img-col3', '', '', '2018-08-22 09:57:53', '2018-08-22 09:57:53', '', 21, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/img-col3.jpg', 0, 'attachment', 'image/jpeg', 0),
(26, 1, '2018-08-22 10:11:12', '2018-08-22 10:11:12', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2018-08-22 10:11:12', '2018-08-22 10:11:12', '', 21, 'http://localhost:8080/projects/triip_me/2018/08/22/21-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2018-08-22 10:17:59', '2018-08-22 10:17:59', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2018-08-22 10:17:59', '2018-08-22 10:17:59', '', 21, 'http://localhost:8080/projects/triip_me/2018/08/22/21-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2018-08-23 02:36:11', '2018-08-23 02:36:11', 'Chúc mừng đến với WordPress. Đây là bài viết đầu tiên của bạn. Hãy chỉnh sửa hay xóa bài viết này, và bắt đầu viết blog!a\n<ul>\n 	<li>adsadsad</li>\n 	<li>asdsadsad</li>\n 	<li>asdsa</li>\n</ul>', 'Chào tất cả mọi người!', '', 'inherit', 'closed', 'closed', '', '1-autosave-v1', '', '', '2018-08-23 02:36:11', '2018-08-23 02:36:11', '', 1, 'http://localhost:8080/projects/triip_me/2018/08/23/1-autosave-v1/', 0, 'revision', '', 0),
(29, 1, '2018-08-23 02:36:14', '2018-08-23 02:36:14', 'Chúc mừng đến với WordPress. Đây là bài viết đầu tiên của bạn. Hãy chỉnh sửa hay xóa bài viết này, và bắt đầu viết blog!a\r\n<ul>\r\n 	<li>adsadsad</li>\r\n 	<li>asdsadsad</li>\r\n 	<li>asdsa</li>\r\n</ul>', 'Chào tất cả mọi người!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-08-23 02:36:14', '2018-08-23 02:36:14', '', 1, 'http://localhost:8080/projects/triip_me/2018/08/23/1-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2018-08-23 02:50:45', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-08-23 02:50:45', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=the-places&p=30', 0, 'the-places', '', 0),
(31, 1, '2018-08-23 03:04:47', '2018-08-23 03:04:47', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:9:\"the-tours\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'thông tin tour', 'thong-tin-tour', 'publish', 'closed', 'closed', '', 'group_5b7e228326b94', '', '', '2018-08-25 09:51:27', '2018-08-25 09:51:27', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=acf-field-group&#038;p=31', 0, 'acf-field-group', '', 0),
(32, 1, '2018-08-23 03:04:47', '2018-08-23 03:04:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"32\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Thời lượng', 'triip_duration', 'publish', 'closed', 'closed', '', 'field_5b7e22bd07349', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=32', 3, 'acf-field', '', 0),
(33, 1, '2018-08-23 03:04:47', '2018-08-23 03:04:47', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"32\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"m/d/Y\";s:13:\"return_format\";s:5:\"d/m/Y\";s:9:\"first_day\";i:1;}', 'Khởi hành', 'triip_departure', 'publish', 'closed', 'closed', '', 'field_5b7e23400734a', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=33', 4, 'acf-field', '', 0),
(34, 1, '2018-08-23 03:04:47', '2018-08-23 03:04:47', 'a:12:{s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"32\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:4:\"step\";s:0:\"\";}', 'Giá cho mỗi thành viên', 'triip_price_per_participant', 'publish', 'closed', 'closed', '', 'field_5b7e236f0734b', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=34', 5, 'acf-field', '', 0),
(35, 1, '2018-08-23 03:04:47', '2018-08-23 03:04:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Ngôn ngữ', 'triip_languages', 'publish', 'closed', 'closed', '', 'field_5b7e24360734c', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=35', 6, 'acf-field', '', 0),
(36, 1, '2018-08-23 03:18:52', '2018-08-23 03:18:52', 'a:12:{s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:4:\"step\";s:0:\"\";}', 'Số người tham gia', 'triip_group_size', 'publish', 'closed', 'closed', '', 'field_5b7e25f8c18fc', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=36', 7, 'acf-field', '', 0),
(37, 1, '2018-08-23 03:18:52', '2018-08-23 03:18:52', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Phương tiên vận chuyển', 'triip_transportation', 'publish', 'closed', 'closed', '', 'field_5b7e2455c18fa', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=37', 8, 'acf-field', '', 0),
(38, 1, '2018-08-23 03:18:52', '2018-08-23 03:18:52', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Dịch vụ của tour', 'triip_include_tour', 'publish', 'closed', 'closed', '', 'field_5b7e2631c18fd', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=38', 9, 'acf-field', '', 0),
(39, 1, '2018-08-23 03:18:52', '2018-08-23 03:18:52', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Những dịch vụ không có', 'triip_exclude_tour', 'publish', 'closed', 'closed', '', 'field_5b7e263dc18fe', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=39', 10, 'acf-field', '', 0),
(40, 1, '2018-08-23 03:22:09', '2018-08-23 03:22:09', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:4;s:9:\"new_lines\";s:0:\"\";}', 'Hủy bỏ', 'triip_cancellation', 'publish', 'closed', 'closed', '', 'field_5b7e283bab0da', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=40', 11, 'acf-field', '', 0),
(41, 1, '2018-08-23 03:22:24', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-08-23 03:22:24', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=the-tours&p=41', 0, 'the-tours', '', 0),
(42, 1, '2018-08-23 03:29:47', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-08-23 03:29:47', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=the-tours&p=42', 0, 'the-tours', '', 0),
(43, 1, '2018-08-23 03:39:27', '2018-08-23 03:39:27', '<section class=\"step\">\r\n<div class=\"inner-wrapper\">\r\n<div class=\"headline-group\">\r\n<h4 class=\"step-headline\">In the morning</h4>\r\n</div>\r\n<div class=\"content-block\">\r\n\r\nyou will be picked up at your hotel then get transferred for 1,5 hours to the Ba Vi National Park. On arrival at the national park, our driver will take you to the relaxing point of 400m above sea level. Afterward, we move to the point of 1,100m above sea level to start trekking to Tan Vien Peak, one of 3 Ba Vi’s highest peaks – 1,226m above sea level then hike up to visit the Upper Temple, where worships the God of Ba Vi Mountain Range, one of The Four Immortals in the traditional Vietnamese mythology. Here, you can capture the most breathtaking panoramic view of the Ba Vi Mountain Range.\r\n\r\nLunch is served at the local restaurant nearby.\r\n\r\n</div>\r\n</div>\r\n<section class=\"highlight-images justified-gallery\" data-pswp-uid=\"1\">\r\n<figure class=\"jg-entry entry-visible\"></figure>\r\n</section></section><section class=\"step\">\r\n<div class=\"inner-wrapper\">\r\n<div class=\"headline-group\">\r\n<h4 class=\"step-headline\">In the afternoon</h4>\r\n</div>\r\n<div class=\"content-block\">\r\n\r\n,get ready for the hardest 2.5km-trekking route from the point of 1,000m down to 800m, which will challenge your ability with a lot of steep slopes as well as slippery roads. During this route, you can see some giant thousand-year-old Old Green Cypresses, one of the world’s rare species preserved in the primeval forest at an altitude of 1,000m and discover the Political Prison, a French ruin built in the early 19th century. Moreover, there are also many special species of birds, reptiles and amphibians waiting for you to discover along the way.\r\n\r\nAfter finishing the trekking tour through Ba Vi National Park, our driver will take you to visit Ba Trai Village, a 500ha-area of tea plantation. On arrival at the village, you will join with the locals to learn a full process of making raw tea leaf products.\r\n\r\nAfter that, a driver will transfer you back to your hotel in Hanoi. End of tour.\r\n\r\n</div>\r\n</div>\r\n<section class=\"highlight-images justified-gallery\" data-pswp-uid=\"2\">\r\n<figure class=\"jg-entry entry-visible\"></figure>\r\n</section></section>', 'Ba Vi Jungle Trekking - (DAILY TOUR)', '', 'publish', 'open', 'closed', '', 'ba-vi-jungle-trekking-daily-tour', '', '', '2018-08-25 10:01:48', '2018-08-25 10:01:48', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=the-tours&#038;p=43', 0, 'the-tours', '', 0),
(44, 1, '2018-08-23 03:45:22', '2018-08-23 03:45:22', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:6:\"visual\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:0;s:5:\"delay\";i:0;}', 'Tóm tắt', 'triip_summary', 'publish', 'closed', 'closed', '', 'field_5b7e2d6896447', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=44', 1, 'acf-field', '', 0),
(45, 1, '2018-08-23 07:58:24', '2018-08-23 07:58:24', '', 'img-col3', '', 'inherit', 'open', 'closed', '', 'img-col3-2', '', '', '2018-08-23 07:58:24', '2018-08-23 07:58:24', '', 0, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/img-col3-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(47, 1, '2018-08-23 08:08:08', '2018-08-23 08:08:08', '', 'Demo', '', 'publish', 'closed', 'closed', '', 'demo', '', '', '2018-08-23 08:08:21', '2018-08-23 08:08:21', '', 0, 'http://localhost:8080/projects/triip_me/?page_id=47', 0, 'page', '', 0),
(48, 1, '2018-08-23 08:08:08', '2018-08-23 08:08:08', '', 'Demo', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2018-08-23 08:08:08', '2018-08-23 08:08:08', '', 47, 'http://localhost:8080/projects/triip_me/2018/08/23/47-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2018-08-23 08:29:56', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-08-23 08:29:56', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/projects/triip_me/?page_id=49', 0, 'page', '', 0),
(50, 1, '2018-08-23 09:33:16', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2018-08-23 09:33:16', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=the-tours&p=50', 0, 'the-tours', '', 0),
(51, 1, '2018-08-23 09:45:26', '2018-08-23 09:45:26', '', 'Tìm kiếm nâng cao', '', 'publish', 'closed', 'closed', '', 'smart-search', '', '', '2018-08-23 09:45:39', '2018-08-23 09:45:39', '', 0, 'http://localhost:8080/projects/triip_me/?page_id=51', 0, 'page', '', 0),
(52, 1, '2018-08-23 09:45:26', '2018-08-23 09:45:26', '', 'Tìm kiếm nâng cao', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2018-08-23 09:45:26', '2018-08-23 09:45:26', '', 51, 'http://localhost:8080/projects/triip_me/2018/08/23/51-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2018-08-24 03:16:06', '2018-08-24 03:16:06', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Địa chỉ', 'triip_location', 'publish', 'closed', 'closed', '', 'field_5b7f78392cace', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=53', 12, 'acf-field', '', 0),
(54, 1, '2018-08-24 03:22:29', '2018-08-24 03:22:29', '', 'single-banner', '', 'inherit', 'open', 'closed', '', 'single-banner', '', '', '2018-08-24 03:22:29', '2018-08-24 03:22:29', '', 43, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/single-banner.jpg', 0, 'attachment', 'image/jpeg', 0),
(55, 1, '2018-08-24 03:26:09', '2018-08-24 03:26:09', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh banner', 'triip_banner', 'publish', 'closed', 'closed', '', 'field_5b7f7ab73052d', '', '', '2018-08-24 07:27:29', '2018-08-24 07:27:29', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=55', 2, 'acf-field', '', 0),
(56, 1, '2018-08-24 03:29:14', '2018-08-24 03:29:14', '', 'item1', '', 'inherit', 'open', 'closed', '', 'item1', '', '', '2018-08-24 03:29:14', '2018-08-24 03:29:14', '', 43, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/item1.jpg', 0, 'attachment', 'image/jpeg', 0),
(57, 1, '2018-08-24 03:29:14', '2018-08-24 03:29:14', '', 'item2', '', 'inherit', 'open', 'closed', '', 'item2', '', '', '2018-08-24 03:29:14', '2018-08-24 03:29:14', '', 43, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/item2.jpg', 0, 'attachment', 'image/jpeg', 0),
(58, 1, '2018-08-24 03:29:15', '2018-08-24 03:29:15', '', 'item8', '', 'inherit', 'open', 'closed', '', 'item8', '', '', '2018-08-24 03:29:15', '2018-08-24 03:29:15', '', 43, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/item8.jpg', 0, 'attachment', 'image/jpeg', 0),
(59, 1, '2018-08-24 03:29:16', '2018-08-24 03:29:16', '', 'item9', '', 'inherit', 'open', 'closed', '', 'item9', '', '', '2018-08-24 03:29:16', '2018-08-24 03:29:16', '', 43, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/item9.jpg', 0, 'attachment', 'image/jpeg', 0),
(60, 1, '2018-08-24 07:16:12', '2018-08-24 07:16:12', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:8:\"taxonomy\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"places\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Ảnh chuyên mục', 'anh-chuyen-muc', 'publish', 'closed', 'closed', '', 'group_5b7faf892d2f5', '', '', '2018-08-24 07:17:02', '2018-08-24 07:17:02', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=acf-field-group&#038;p=60', 0, 'acf-field-group', '', 0),
(61, 1, '2018-08-24 07:16:12', '2018-08-24 07:16:12', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh đại diện khu du lịch', 'triip_thumbnail_places', 'publish', 'closed', 'closed', '', 'field_5b7fafa276201', '', '', '2018-08-24 07:17:02', '2018-08-24 07:17:02', '', 60, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=61', 0, 'acf-field', '', 0),
(62, 1, '2018-08-24 07:27:29', '2018-08-24 07:27:29', 'a:12:{s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:3:\"min\";i:1;s:3:\"max\";i:10;s:4:\"step\";s:0:\"\";}', 'Review Score', 'triip_review_score', 'publish', 'closed', 'closed', '', 'field_5b7fb32a250d4', '', '', '2018-08-25 09:51:27', '2018-08-25 09:51:27', '', 31, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=62', 0, 'acf-field', '', 0),
(63, 1, '2018-08-24 07:42:01', '2018-08-24 07:42:01', '<section class=\"step\">\r\n<div class=\"inner-wrapper\">\r\n<div class=\"headline-group\">\r\n<h4 class=\"step-headline\">In the morning</h4>\r\n</div>\r\n<div class=\"content-block\">\r\n\r\nyou will be picked up at your hotel then get transferred for 1,5 hours to the Ba Vi National Park. On arrival at the national park, our driver will take you to the relaxing point of 400m above sea level. Afterward, we move to the point of 1,100m above sea level to start trekking to Tan Vien Peak, one of 3 Ba Vi’s highest peaks – 1,226m above sea level then hike up to visit the Upper Temple, where worships the God of Ba Vi Mountain Range, one of The Four Immortals in the traditional Vietnamese mythology. Here, you can capture the most breathtaking panoramic view of the Ba Vi Mountain Range.\r\n\r\nLunch is served at the local restaurant nearby.\r\n\r\n</div>\r\n</div>\r\n<section class=\"highlight-images justified-gallery\" data-pswp-uid=\"1\">\r\n<figure class=\"jg-entry entry-visible\"></figure>\r\n</section></section><section class=\"step\">\r\n<div class=\"inner-wrapper\">\r\n<div class=\"headline-group\">\r\n<h4 class=\"step-headline\">In the afternoon</h4>\r\n</div>\r\n<div class=\"content-block\">\r\n\r\n,get ready for the hardest 2.5km-trekking route from the point of 1,000m down to 800m, which will challenge your ability with a lot of steep slopes as well as slippery roads. During this route, you can see some giant thousand-year-old Old Green Cypresses, one of the world’s rare species preserved in the primeval forest at an altitude of 1,000m and discover the Political Prison, a French ruin built in the early 19th century. Moreover, there are also many special species of birds, reptiles and amphibians waiting for you to discover along the way.\r\n\r\nAfter finishing the trekking tour through Ba Vi National Park, our driver will take you to visit Ba Trai Village, a 500ha-area of tea plantation. On arrival at the village, you will join with the locals to learn a full process of making raw tea leaf products.\r\n\r\nAfter that, a driver will transfer you back to your hotel in Hanoi. End of tour.\r\n\r\n</div>\r\n</div>\r\n<section class=\"highlight-images justified-gallery\" data-pswp-uid=\"2\">\r\n<figure class=\"jg-entry entry-visible\"></figure>\r\n</section></section>', '3 Hours Ride to Hanoi Countryside', '', 'publish', 'open', 'closed', '', '3-hours-ride-to-hanoi-countryside', '', '', '2018-08-25 09:51:43', '2018-08-25 09:51:43', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=the-tours&#038;p=63', 0, 'the-tours', '', 4),
(65, 1, '2018-08-24 10:22:22', '2018-08-24 10:22:22', '', 'Default Form', '', 'publish', 'closed', 'closed', '', 'default-form', '', '', '2018-08-24 10:49:49', '2018-08-24 10:49:49', '', 0, 'http://localhost:8080/projects/triip_me/2018/08/24/default-form/', 0, 'wpdiscuz_form', '', 0),
(66, 1, '2018-08-27 02:23:03', '2018-08-27 02:23:03', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"21\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Trang chủ', 'trang-chu', 'publish', 'closed', 'closed', '', 'group_5b83604403bbb', '', '', '2018-08-27 08:34:49', '2018-08-27 08:34:49', '', 0, 'http://localhost:8080/projects/triip_me/?post_type=acf-field-group&#038;p=66', 0, 'acf-field-group', '', 0),
(67, 1, '2018-08-27 02:23:03', '2018-08-27 02:23:03', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Banner', 'triip_banner', 'publish', 'closed', 'closed', '', 'field_5b83604a35145', '', '', '2018-08-27 08:28:00', '2018-08-27 08:28:00', '', 66, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=67', 1, 'acf-field', '', 0),
(68, 1, '2018-08-27 02:24:23', '2018-08-27 02:24:23', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner', '', '', '2018-08-27 02:24:23', '2018-08-27 02:24:23', '', 21, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/banner.jpg', 0, 'attachment', 'image/jpeg', 0),
(69, 1, '2018-08-27 02:24:44', '2018-08-27 02:24:44', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2018-08-27 02:24:44', '2018-08-27 02:24:44', '', 21, 'http://localhost:8080/projects/triip_me/2018/08/27/21-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2018-08-27 08:24:26', '2018-08-27 08:24:26', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2018-08-27 08:24:26', '2018-08-27 08:24:26', '', 21, 'http://localhost:8080/projects/triip_me/2018/08/27/21-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2018-08-27 08:28:00', '2018-08-27 08:28:00', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:3;s:9:\"new_lines\";s:0:\"\";}', 'Home Title', 'triip_home_title', 'publish', 'closed', 'closed', '', 'field_5b83b5eae0bb3', '', '', '2018-08-27 08:34:49', '2018-08-27 08:34:49', '', 66, 'http://localhost:8080/projects/triip_me/?post_type=acf-field&#038;p=71', 0, 'acf-field', '', 0),
(72, 1, '2018-08-27 08:31:10', '2018-08-27 08:31:10', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2018-08-27 08:31:10', '2018-08-27 08:31:10', '', 21, 'http://localhost:8080/projects/triip_me/2018/08/27/21-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2018-08-27 09:24:14', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-08-27 09:24:14', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/projects/triip_me/?page_id=73', 0, 'page', '', 0),
(74, 1, '2018-08-27 09:54:14', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-08-27 09:54:14', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/projects/triip_me/?page_id=74', 0, 'page', '', 0),
(75, 1, '2018-08-29 06:31:57', '2018-08-29 06:31:57', '', '40221133_301201817353120_7401667735635099648_n', '', 'inherit', 'open', 'closed', '', '40221133_301201817353120_7401667735635099648_n', '', '', '2018-08-29 06:31:57', '2018-08-29 06:31:57', '', 0, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/40221133_301201817353120_7401667735635099648_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(76, 1, '2018-08-29 06:32:14', '2018-08-29 06:32:14', 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/cropped-40221133_301201817353120_7401667735635099648_n.jpg', 'cropped-40221133_301201817353120_7401667735635099648_n.jpg', '', 'inherit', 'open', 'closed', '', 'cropped-40221133_301201817353120_7401667735635099648_n-jpg', '', '', '2018-08-29 06:32:14', '2018-08-29 06:32:14', '', 0, 'http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/cropped-40221133_301201817353120_7401667735635099648_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(77, 1, '2018-08-29 06:32:18', '2018-08-29 06:32:18', '{\n    \"triip me::custom_logo\": {\n        \"value\": 76,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2018-08-29 06:32:18\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '2061f483-6caa-4ea5-a0d5-2b2b4de542a7', '', '', '2018-08-29 06:32:18', '2018-08-29 06:32:18', '', 0, 'http://localhost:8080/projects/triip_me/2018/08/29/2061f483-6caa-4ea5-a0d5-2b2b4de542a7/', 0, 'customize_changeset', '', 0),
(78, 1, '2018-08-29 06:38:58', '2018-08-29 06:38:58', '', 'Liên hệ', '', 'publish', 'closed', 'closed', '', 'lien-he', '', '', '2018-08-29 06:45:50', '2018-08-29 06:45:50', '', 0, 'http://localhost:8080/projects/triip_me/?page_id=78', 0, 'page', '', 0),
(79, 1, '2018-08-29 06:38:58', '2018-08-29 06:38:58', '', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '78-revision-v1', '', '', '2018-08-29 06:38:58', '2018-08-29 06:38:58', '', 78, 'http://localhost:8080/projects/triip_me/2018/08/29/78-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_termmeta`
--

CREATE TABLE `triip_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_terms`
--

CREATE TABLE `triip_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `triip_terms`
--

INSERT INTO `triip_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Chưa được phân loại', 'khong-phan-loai', 0),
(2, 'Việt Nam', 'viet-nam', 0),
(3, 'Hà Nội', 'ha-noi', 0),
(4, 'Hồ Chí Minh', 'ho-chi-minh', 0),
(5, 'Đà Nẵng', 'da-nang', 0),
(6, 'Thái Lan', 'thai-lan', 0),
(7, 'Bangkok', 'bangkok', 0),
(8, 'Phuket', 'phuket', 0),
(9, 'Pattaya', 'pattaya', 0),
(10, 'Chiang Mai', 'chiang-mai', 0),
(11, 'Family &amp; friend', 'family-friend', 0),
(12, 'Food &amp; beverage', 'food-beverage', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_term_relationships`
--

CREATE TABLE `triip_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `triip_term_relationships`
--

INSERT INTO `triip_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(43, 3, 0),
(43, 11, 0),
(43, 12, 0),
(63, 3, 0),
(63, 11, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_term_taxonomy`
--

CREATE TABLE `triip_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `triip_term_taxonomy`
--

INSERT INTO `triip_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'places', '', 0, 0),
(3, 3, 'places', '', 2, 2),
(4, 4, 'places', '', 2, 0),
(5, 5, 'places', '', 2, 0),
(6, 6, 'places', '', 0, 0),
(7, 7, 'places', '', 6, 0),
(8, 8, 'places', '', 6, 0),
(9, 9, 'places', '', 6, 0),
(10, 10, 'places', '', 6, 0),
(11, 11, 'the-categories', '', 0, 2),
(12, 12, 'the-categories', '', 0, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_usermeta`
--

CREATE TABLE `triip_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `triip_usermeta`
--

INSERT INTO `triip_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'false'),
(11, 1, 'locale', ''),
(12, 1, 'triip_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'triip_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'triip_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'triip_user-settings', 'libraryContent=browse&mfold=o&editor=tinymce'),
(19, 1, 'triip_user-settings-time', '1535093921'),
(20, 1, 'acf_user_settings', 'a:0:{}'),
(21, 1, 'closedpostboxes_the-tours', 'a:0:{}'),
(22, 1, 'metaboxhidden_the-tours', 'a:5:{i:0;s:23:\"acf-group_5b7d1e5dd332a\";i:1;s:23:\"acf-group_5b7d2f09cad9d\";i:2;s:11:\"postexcerpt\";i:3;s:16:\"commentstatusdiv\";i:4;s:7:\"slugdiv\";}'),
(26, 1, 'session_tokens', 'a:24:{s:64:\"2066d88150eee068cfc252a65ad2773e356801cc5a0dc95fafb7d649d94f85d5\";a:4:{s:10:\"expiration\";i:1536357472;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535147872;}s:64:\"29ca2e6a28d87cb3401d9e67b1f298c55f88de555552f2631fdb43be6d3e7e46\";a:4:{s:10:\"expiration\";i:1536357516;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535147916;}s:64:\"c693a686cf75f4205edcca7b9c3e0fec0779194313b43253ff5852afffce860b\";a:4:{s:10:\"expiration\";i:1536357523;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535147923;}s:64:\"5c092341577f663bbdbe030f08b8a68aa0a1edcf2a11b2b74966a7c742944c49\";a:4:{s:10:\"expiration\";i:1536390091;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180491;}s:64:\"d2959a88e3a4ee4c5391c01fa78d8549c3117b040d14ecbc43e408d808fec154\";a:4:{s:10:\"expiration\";i:1536390179;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180579;}s:64:\"615fcc96944dbf0343402c7fc6325178eea36b83e1442e7aa9fa0c46324e891d\";a:4:{s:10:\"expiration\";i:1536390180;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180580;}s:64:\"7855b51049e74fb59731f2f363d4fd86af08aaa662257d787c98f2f723b0f2b0\";a:4:{s:10:\"expiration\";i:1536390182;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180582;}s:64:\"60b18980592dc6e0aa5511f336271e3fcef8d36c0449a22163d8eae173a9a6e8\";a:4:{s:10:\"expiration\";i:1536390183;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180583;}s:64:\"845a9da96d55ec3ffbb374160c6b39bc30889ea6277ad0aeee935dc161077aa4\";a:4:{s:10:\"expiration\";i:1536390184;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180584;}s:64:\"012c260c30ee50f63491b58829f22866be43190616b3651d4a86a4de3bba10dc\";a:4:{s:10:\"expiration\";i:1536390185;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180585;}s:64:\"ee5dadc2e42a778125962d43e3d7a1c0ce03df12675d64ea7ad8f2b8f12019dc\";a:4:{s:10:\"expiration\";i:1536390186;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180586;}s:64:\"6c0d957c46157249e26548014d0735131e54bad3139d5d975f7375b5116b2764\";a:4:{s:10:\"expiration\";i:1536390188;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180588;}s:64:\"a498d7012091477cbd4172062e6391d3f24fda4cf69095233d6b80d7d1fdf954\";a:4:{s:10:\"expiration\";i:1536390189;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180589;}s:64:\"b38225643384e626fad8a2b3e4638e3840b4fc95e6649be80d30e7ca470689e8\";a:4:{s:10:\"expiration\";i:1536390200;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180600;}s:64:\"de93365e945d00c62f3c4a7e19d526e6bb92f121cf828d2255491cad909f9469\";a:4:{s:10:\"expiration\";i:1536390215;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180615;}s:64:\"84c6beb1a6dab9cafcfca90cab26b2aa44d2e9228118ca3dcffe0981f60ebb55\";a:4:{s:10:\"expiration\";i:1536390235;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180635;}s:64:\"f3b9c45b1e10e1a3ac5e18271af3ae1e73ec471e98c5daea73a632e6498dc817\";a:4:{s:10:\"expiration\";i:1536390279;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180679;}s:64:\"627106fa5f40ed04b60d218860fc6ac66f7b414160df3beb1498ec75352544e5\";a:4:{s:10:\"expiration\";i:1536390338;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180738;}s:64:\"7480f7e2ebd763d561bc4de6fceb3d2488633b39c89594e4073986dcd0569c3a\";a:4:{s:10:\"expiration\";i:1536390383;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535180783;}s:64:\"98d1cd04c193f93dc525c2c66710e41d50f703fbfa65f32f7033319a3866ac57\";a:4:{s:10:\"expiration\";i:1536390872;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535181272;}s:64:\"c32c2ee30b738c5b6f7c15276e355d4924219f1c08092ce2cf6491b38de49e30\";a:4:{s:10:\"expiration\";i:1536390955;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535181355;}s:64:\"e01b6e6be7b54ad45fb71f7452ed5d74f0caa4eb47d18f5f1f1ec055b56ff6ad\";a:4:{s:10:\"expiration\";i:1536397475;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535187875;}s:64:\"11bba4c69a42f5c6cfaaa28cab8d32a6be15b34b17c37f78d82394c6ed0828b9\";a:4:{s:10:\"expiration\";i:1535670082;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535497282;}s:64:\"d6ae6767a7159397857907856237dd81220fbef668fcdd7f7a3fc532033b743d\";a:4:{s:10:\"expiration\";i:1536733839;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535524239;}}'),
(27, 2, 'nickname', 'satthugiap9x@gmail.com'),
(28, 2, 'first_name', ''),
(29, 2, 'last_name', ''),
(30, 2, 'description', ''),
(31, 2, 'rich_editing', 'true'),
(32, 2, 'syntax_highlighting', 'true'),
(33, 2, 'comment_shortcuts', 'false'),
(34, 2, 'admin_color', 'fresh'),
(35, 2, 'use_ssl', '0'),
(36, 2, 'show_admin_bar_front', 'false'),
(37, 2, 'locale', ''),
(38, 2, 'triip_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(39, 2, 'triip_user_level', '0'),
(41, 2, 'wp_user_avatars', 'a:10:{s:4:\"full\";s:95:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_2_1535335496.jpg\";i:128;s:103:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_2_1535335496-128x128.jpg\";i:64;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_2_1535335496-64x64.jpg\";i:52;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_2_1535335496-52x52.jpg\";i:26;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_2_1535335496-26x26.jpg\";i:192;s:103:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_2_1535335496-192x192.jpg\";i:96;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_2_1535335496-96x96.jpg\";i:500;s:103:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_2_1535335496-500x500.jpg\";i:250;s:103:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_2_1535335496-250x250.jpg\";i:32;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_2_1535335496-32x32.jpg\";}'),
(42, 2, 'wp_user_avatars_rating', 'G'),
(43, 2, 'session_tokens', 'a:2:{s:64:\"a2137b83e3aa8ddbd824675c609827df80d9cdf1e7211f1a0603660d33fd5ec1\";a:4:{s:10:\"expiration\";i:1536545391;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535335791;}s:64:\"4f528de25ffbd50b58cd1ff0b0bb798ddb2ec569137cc6f717bb007ad3c5cdbc\";a:4:{s:10:\"expiration\";i:1536733876;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535524276;}}'),
(44, 1, 'wp_user_avatars', 'a:14:{s:4:\"full\";s:95:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231.jpg\";i:128;s:103:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-128x128.jpg\";i:64;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-64x64.jpg\";i:52;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-52x52.jpg\";i:26;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-26x26.jpg\";i:192;s:103:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-192x192.jpg\";i:96;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-96x96.jpg\";i:500;s:103:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-500x500.jpg\";i:250;s:103:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-250x250.jpg\";i:48;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-48x48.jpg\";i:24;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-24x24.jpg\";i:100;s:103:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-100x100.jpg\";i:50;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-50x50.jpg\";i:32;s:101:\"http://localhost:8080/projects/triip_me/wp-content/uploads/2018/08/avatar_user_1_1535336231-32x32.jpg\";}'),
(45, 1, 'wp_user_avatars_rating', 'G');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_users`
--

CREATE TABLE `triip_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `triip_users`
--

INSERT INTO `triip_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BsCh6amDmdmaavIFvqpXFtWp9fEp0i/', 'admin', 'nguyentronggiap97@gmail.com', '', '2018-08-22 04:31:47', '', 0, 'admin'),
(2, 'satthugiap9x@gmail.com', '$P$BHzSd76IAi7zcwsYlto8FqWkKuVX.E1', 'nguyen-trong-giap', 'satthugiap9x@gmail.com', '', '2018-08-27 02:01:29', '', 0, 'satthugiap9x@gmail.com');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_wc_avatars_cache`
--

CREATE TABLE `triip_wc_avatars_cache` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_email` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `maketime` int(11) NOT NULL DEFAULT '0',
  `cached` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_wc_comments_subscription`
--

CREATE TABLE `triip_wc_comments_subscription` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subscribtion_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `subscribtion_type` varchar(255) NOT NULL,
  `activation_key` varchar(255) NOT NULL,
  `confirm` tinyint(4) DEFAULT '0',
  `subscription_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_wc_follow_users`
--

CREATE TABLE `triip_wc_follow_users` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_email` varchar(125) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `follower_id` int(11) NOT NULL DEFAULT '0',
  `follower_email` varchar(125) NOT NULL,
  `follower_name` varchar(255) NOT NULL,
  `activation_key` varchar(32) NOT NULL,
  `confirm` tinyint(1) NOT NULL DEFAULT '0',
  `follow_timestamp` int(11) NOT NULL,
  `follow_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_wc_phrases`
--

CREATE TABLE `triip_wc_phrases` (
  `id` int(11) NOT NULL,
  `phrase_key` varchar(255) NOT NULL,
  `phrase_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `triip_wc_users_voted`
--

CREATE TABLE `triip_wc_users_voted` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `vote_type` int(11) DEFAULT NULL,
  `is_guest` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `triip_commentmeta`
--
ALTER TABLE `triip_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `triip_comments`
--
ALTER TABLE `triip_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Chỉ mục cho bảng `triip_links`
--
ALTER TABLE `triip_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Chỉ mục cho bảng `triip_options`
--
ALTER TABLE `triip_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Chỉ mục cho bảng `triip_postmeta`
--
ALTER TABLE `triip_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `triip_posts`
--
ALTER TABLE `triip_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Chỉ mục cho bảng `triip_termmeta`
--
ALTER TABLE `triip_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `triip_terms`
--
ALTER TABLE `triip_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Chỉ mục cho bảng `triip_term_relationships`
--
ALTER TABLE `triip_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Chỉ mục cho bảng `triip_term_taxonomy`
--
ALTER TABLE `triip_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Chỉ mục cho bảng `triip_usermeta`
--
ALTER TABLE `triip_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Chỉ mục cho bảng `triip_users`
--
ALTER TABLE `triip_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Chỉ mục cho bảng `triip_wc_avatars_cache`
--
ALTER TABLE `triip_wc_avatars_cache`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_email` (`user_email`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `url` (`url`),
  ADD KEY `hash` (`hash`),
  ADD KEY `maketime` (`maketime`),
  ADD KEY `cached` (`cached`);

--
-- Chỉ mục cho bảng `triip_wc_comments_subscription`
--
ALTER TABLE `triip_wc_comments_subscription`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subscribe_unique_index` (`subscribtion_id`,`email`,`post_id`),
  ADD KEY `subscribtion_id` (`subscribtion_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `confirm` (`confirm`);

--
-- Chỉ mục cho bảng `triip_wc_follow_users`
--
ALTER TABLE `triip_wc_follow_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `follow_unique_key` (`user_email`,`follower_email`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_email` (`user_email`),
  ADD KEY `follower_id` (`follower_id`),
  ADD KEY `follower_email` (`follower_email`),
  ADD KEY `confirm` (`confirm`),
  ADD KEY `follow_timestamp` (`follow_timestamp`);

--
-- Chỉ mục cho bảng `triip_wc_phrases`
--
ALTER TABLE `triip_wc_phrases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phrase_key` (`phrase_key`);

--
-- Chỉ mục cho bảng `triip_wc_users_voted`
--
ALTER TABLE `triip_wc_users_voted`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `vote_type` (`vote_type`),
  ADD KEY `is_guest` (`is_guest`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `triip_commentmeta`
--
ALTER TABLE `triip_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `triip_comments`
--
ALTER TABLE `triip_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `triip_links`
--
ALTER TABLE `triip_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `triip_options`
--
ALTER TABLE `triip_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=384;

--
-- AUTO_INCREMENT cho bảng `triip_postmeta`
--
ALTER TABLE `triip_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=326;

--
-- AUTO_INCREMENT cho bảng `triip_posts`
--
ALTER TABLE `triip_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT cho bảng `triip_termmeta`
--
ALTER TABLE `triip_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `triip_terms`
--
ALTER TABLE `triip_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `triip_term_taxonomy`
--
ALTER TABLE `triip_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `triip_usermeta`
--
ALTER TABLE `triip_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT cho bảng `triip_users`
--
ALTER TABLE `triip_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `triip_wc_avatars_cache`
--
ALTER TABLE `triip_wc_avatars_cache`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `triip_wc_comments_subscription`
--
ALTER TABLE `triip_wc_comments_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `triip_wc_follow_users`
--
ALTER TABLE `triip_wc_follow_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `triip_wc_phrases`
--
ALTER TABLE `triip_wc_phrases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `triip_wc_users_voted`
--
ALTER TABLE `triip_wc_users_voted`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
