<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define('DB_NAME', 'project_triip-me');

/** Username của database */
define('DB_USER', 'root');

/** Mật khẩu của database */
define('DB_PASSWORD', '');

/** Hostname của database */
define('DB_HOST', 'localhost');

/** Database charset sử dụng để tạo bảng database. */
define('DB_CHARSET', 'utf8mb4');

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ugT^ Fb[mUmPmDN(qo .2eAloIB,mCDZK2RhL,psVOjJo%lBM-I8XjP5 qo32/^#');
define('SECURE_AUTH_KEY',  'HM2&UW]20a1v%Yx_BR/1y{y}4Jn|<{+47KNg4tl,NIgO8^:*eRR}o3u:@TV{wJq2');
define('LOGGED_IN_KEY',    'f+#_GzQ>Xh6Ap >XbP9A;Hu|um*5g!IhuUS*.85*QhYd7%Vrn=k,HS{YI&NIL#i$');
define('NONCE_KEY',        'eKwcbXcDb9}?u^X&Uq,OoF196XJmg5QTlDw=YF-iLo&CU?ur]m.&d#>JeF5 ~Cc ');
define('AUTH_SALT',        'I?~9G-?y[^`?fNn{YZNPk28(KqB/ZOtcJUbvq~qi6wvY.{A~w%8J4?]ZKRJ.)YkC');
define('SECURE_AUTH_SALT', '.7?J1VHdu:OHlDW803N{490gz*rUHPX*|x7#9=EfTYH5e $J?Q8)y)WG:W4MicTE');
define('LOGGED_IN_SALT',   '}1#2rs%Lae=4LD;3?HA([pQmyO Wg9U,&ARAssof$#$/wC_lPEIlQk=01PO}TKoT');
define('NONCE_SALT',       '`jMcxrIv6zu_8Gp^0,^WV[QJRMAu>,h/$O}01`Sfq6~p %-P6&O#0nWlK1sqtm{!');

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix  = 'triip_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
