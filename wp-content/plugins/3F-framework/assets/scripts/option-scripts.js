jQuery(document).ready(function($) {
    /**
     * Ajax loader when scroll to bottom
     * must be definded "action" in class.ajax-functions.php
     */

    $('#save-options').click(function(e){
        e.preventDefault();
        console.log();
        $.ajax({
            type: 'POST',
            data: $('#options-form').serialize()+'&action=save_options&'+'nonce='+WPAjax.nonce,
            url: WPAjax.url,
            beforeSend: function() {
                $('.wp-heading-inline').append('<span id="loading" class="spinner is-active"></span>');
                $('#save-options').text('Đang lưu...');
            },
            success: function(data) {
                $('#loading').remove();
                if(data.data) {
                    $('#save-options').text('Đã lưu');
                    setTimeout(() => {
                        $('#save-options').text('Cập nhật');
                    }, 1000); 
                } else {
                    $('#save-options').text('Có lỗi!');
                }
            }
        });
    })
});