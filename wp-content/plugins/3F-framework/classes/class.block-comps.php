<?php 
/**
 * Block important component in admin page
 */

 class TF_BlockCategories {

     private $undeletable;

     function __construct($catIDs) {
        $this->undeletable = $catIDs;

        add_action( 'delete_term_taxonomy', array($this,'del_taxonomy'), 10, 1 );
        add_action( 'edit_term_taxonomies', array($this,'del_child_tax'), 10, 1 );
        add_filter( "category_row_actions", array($this, 'category_row_actions'), 10, 2 );
     }

    function category_row_actions( $actions, $tag ) {
        if ( in_array($tag->term_id,$this->undeletable)) unset( $actions['delete'] );
        return $actions;
    }
    
    function del_taxonomy( $tt_id ) {
        $term = get_term_by( 'id', $tt_id, 'category' );
        if( in_array( $term->term_id, $this->undeletable ) ) 
        wp_die( 'Cant delete this category!' );
    }

    function del_child_tax( $arr_ids ) {
        foreach( $arr_ids as $id ){
            $term   = get_term_by( 'id', $id, 'category' );
            $parent = get_term_by( 'id', $term->parent, 'category' );
            if( in_array( $parent->term_id, $this->undeletable ) ) 
                wp_die( 'Cant delete this category!' );
        }
    }
 }



 class TF_BlockPosts {

    private $postIDs;

    function __construct($post_ids) {
       $this->postIDs = $post_ids;
       add_action('wp_trash_post', array($this,'restrict_post_deletion'), 10, 1);
       add_action('wp_delete_post', array($this,'restrict_post_deletion'), 10, 1);
       add_filter( "page_row_actions", array($this, 'post_row_actions'), 10, 2 );
       add_filter( "post_row_actions", array($this, 'post_row_actions'), 10, 2 );
    }
    
    function post_row_actions( $actions, $post ) {

        if ( in_array($post->ID,$this->postIDs)) unset( $actions['trash'] );
        return $actions;
    }

    function restrict_post_deletion($post_ID){
       if(in_array($post_ID, $this->postIDs)){
           $post_type = get_post_type($post_ID);
           $url_redirect = $post_type=='page'?'post_type=page':'';
           echo "<h2>This is an important post. You can not remove it. If you try to remove it, maybe you can get an error on your site!</h2>";
           echo '<script>
               setTimeout(() => {
                   window.location = "'. admin_url('edit.php?'.$url_redirect) .'";
               }, 3000);
           </script>';
           die();
       }
   }
}

function tw_remove_page_editor() {
    if ( is_admin() ) {
        if(isset($_GET['post']) && in_array($_GET['post'],get_option('framework_remove-page-editor'))) {
            remove_post_type_support('page', 'editor');
        }
    }
}
add_action( 'init', 'tw_remove_page_editor' );

new TF_BlockCategories(get_option('framework_block-categories'));
new TF_BlockPosts(get_option('framework_block-pages'));