<?php
/**
 * twtheme Theme Customizer
 *
 * @package WP Themes
 * @subpackage twtheme
 * @since 1.0.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */

class TF_Customize {

    public function __construct($args) {
        if(empty($args)) {
            _e('ERROR: Empty parameter for TF_Customizes class construct');
            die();
        }

        //add customizes
        $this->customize_rigister($args);
    }

    //reigister customizes
    public function customize_rigister($args) {

        add_action( 'customize_register', 'tw_customize_register' );
        function tw_customize_register( $wp_customize ) {
        
            //remove option static option page and custom css
            $wp_customize->remove_section( 'static_front_page' );
            $wp_customize->remove_section( 'custom_css' );
        
            /**
             * Header Settings Panel on customizer
             *
             * @since 1.0.0
             */

            global $args;

            foreach($args as $panel_slug =>  $the_panel) {
                //load panel
                //if($panel_slug = 'name') continue;

                $wp_customize->add_panel(
                    $panel_slug, 
                    array(
                        'priority'       => $the_panel['priority'],
                        'capability'     => 'edit_theme_options',
                        'theme_supports' => '',
                        'title'          => $the_panel['name'],
                    ) 
                );


                foreach($the_panel as $section_slug => $the_section) {
                    
                    if($section_slug == 'name' || $section_slug == 'priority') continue;
                     //load section
                    $wp_customize->add_section(
                        $section_slug,
                        array(
                            'title'		=> $the_section['name'],
                            'panel'     => $panel_slug,
                            'description' => isset($the_section['description'])?$the_section['description']:'',
                            'priority'  => 5,
                        )
                    );

                    foreach($the_section as $setting_slug => $the_setting) {

                        if($setting_slug == 'name'|| $setting_slug == 'description') continue;

                        // //load settings
                        $wp_customize->add_setting(
                            $setting_slug, 
                            array(
                                'default' => $the_setting['default'],
                            )
                        );

                        switch($the_setting['type']) {
                            case 'text': case 'textarea': case 'checkbox': case 'number': case 'date':
                                $wp_customize->add_control(
                                    $setting_slug,
                                    array(
                                        'type' => $the_setting['type'],
                                        'label' =>  $the_setting['name'],
                                        'section' => $section_slug,
                                    )
                                );
                            break;

                            case 'select': case 'radio': 
                                $wp_customize->add_control(
                                    $setting_slug,
                                    array(
                                        'type' => $the_setting['type'],
                                        'label' =>  $the_setting['name'],
                                        'section' => $section_slug,
                                        'choices'=> $the_setting['choices'],
                                    )
                                );
                            break;

                            case 'image': 
                                $wp_customize->add_control(
                                    new WP_Customize_Image_Control(
                                        $wp_customize,
                                        $setting_slug,
                                        array(
                                            'label' =>  $the_setting['name'],
                                            'section' => $section_slug,
                                            'settings'   => $setting_slug,
                                            'context'    => $setting_slug 
                                        )
                                    )
                                );
                            break;


                        } //end swicht

                    } //end  third loop
                } //end second loop
            } //and fisrt loop

        }
    }
}


