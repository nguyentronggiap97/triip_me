<?php

class TF_Hooks {
    function __construct() {
        //varible for ajax
        add_action( 'admin_head', array($this,'ajax_var'));
        add_action( 'wp_head', array($this,'ajax_var'));
        add_action( 'init', array($this,'disable_emojis' ));
        add_filter( 'nav_menu_link_attributes', array($this,'wpd_nav_menu_link_atts'), 20, 4 );
        //change admin text 
        //add_filter('admin_footer_text', array($this,'change_footer_admin'), 9999);
    }

    function ajax_var() { ?>
         <script type="text/javascript">
            var WPAjax = {
                url:'<?php echo admin_url( 'admin-ajax.php' ) ?>',
                nonce: '<?php echo wp_create_nonce("3fwp_nonce" ) ?>',
                homeUrl: '<?php echo home_url() ?>',
                themeUrl: '<?php echo TFT_URL ?>',
            }
        </script>
    <?php }

    function disable_emojis() {
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );    
        remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
        remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );      
        remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
        add_filter( 'tiny_mce_plugins', array($this,'disable_emojis_tinymce' ));
    }

    function disable_emojis_tinymce( $plugins ) {
        if ( is_array( $plugins ) ) {
                return array_diff( $plugins, array( 'wpemoji' ) );
        } else {
                return array();
        }
    }

    // function change_footer_admin() {
    //     return __('Cảm ơn bạn đã sử dụng dịch vụ thiết kế website của <a href="https://3fgroup.vn">3F Group</a>', 'twtheme');
    // }

    
    function wpd_nav_menu_link_atts( $atts, $item, $args, $depth ){
        $_string = 'http://%home%';
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $_string = 'https://%home%';
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
            $_string = 'https://%home%';
        }

        $atts['href'] = str_replace($_string, home_url(), $atts['href']);
        
        return $atts;
    }
}

new TF_Hooks();


