<?php 
/**
* The class make a wordpress admin pages easier
* 
*/

class WP_Ajax {

    private $ajax_id;
    private $processing_file;
    //initialization page
    public function __construct($processing_file ,$ajax_id) {
        $this->ajax_id = $ajax_id;
        $this->processing_file = $processing_file;
        add_action( 'wp_ajax_'.$ajax_id, array(&$this, 'load_processing'));
        add_action( 'wp_ajax_nopriv_'.$ajax_id, array(&$this, 'load_processing'));
    }

    //load file
    public function load_processing() {
        //check nonce
        check_ajax_referer( '3fwp_nonce', 'nonce' );
        require $this->processing_file;
        die();
    }
}



