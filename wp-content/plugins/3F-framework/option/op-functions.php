<?php 
/**
* the class WP_Options
* @author: Trieu Tai Niem
* @package: 3F Wordpress Core
* @link: https://3fgroup.vn
* 
*/

function tw_option_show_options($current_section) {
    foreach($current_section as $sid => $svalue) {
        if(!is_array($svalue)) continue; ?>
        <li id="<?php echo $sid ?>">
            <?php
                $the_theme_mod = get_theme_mod($sid, $svalue['default'] );
                switch($svalue['type']) {

                //textarea type
                case 'textarea': ?>
                    <label class="label block" for="<?php echo $sid ?>"><?php echo $svalue['name'] ?>: </label>
                    <?php if ( isset( $svalue['description']) ) { ?>
                        <small><?php echo wp_kses_post( $svalue['description']); ?></small>
                    <?php } ?>
                    <textarea class="control" rows="<?php echo $svalue['row'] ?>" name="<?php echo $sid ?>"><?php echo $the_theme_mod ?></textarea>
                <?php break;

                //date, number, text type
                case 'number': case 'date': case 'text': ?>
                    <label class="label block" for="<?php echo $sid ?>"><?php echo $svalue['name'] ?>: </label>
                    <?php if ( isset( $svalue['description']) ) { ?>
                        <small><?php echo wp_kses_post( $svalue['description']); ?></small>
                    <?php } ?>
                    <input class="control" id="<?php echo $sid ?>" type="<?php echo $svalue['type'] ?>" name="<?php echo $sid ?>" value="<?php echo $the_theme_mod ?>"/>
                <?php break;

                //checkbox type
                case 'checkbox':?>
                    <input id="<?php echo $sid ?>" type="<?php echo $svalue['type'] ?>" name="<?php echo $sid ?>" value="<?php echo $the_theme_mod ?>" <?php if($svalue['default'] == $the_theme_mod) echo 'checked' ?>/>
                    <label for="<?php echo $sid ?>"><?php echo $svalue['name'] ?></label>
                <?php break;

                //checkbox type
                case 'radio': 
                    echo '<p class="label">'.$svalue['name'].':</p>';
                    if ( isset( $svalue['description']) ) { ?>
                        <small><?php echo wp_kses_post( $svalue['description']); ?></small>
                    <?php }
                    foreach($svalue['choices'] as $choose_id => $choose_vl) { ?>
                        <input id="<?php echo $choose_id ?>" type="<?php echo $svalue['type'] ?>" name="<?php echo $sid ?>" value="<?php echo $choose_id ?>" <?php if($choose_id == $the_theme_mod) echo 'checked' ?>/>
                        <label for="<?php echo $choose_id ?>"><?php echo $choose_vl ?></label>
                <?php } break;

                //checkbox type
                case 'select': 
                    echo '<p class="label">'.$svalue['name'].':</p>';
                    if ( isset( $svalue['description']) ) { ?>
                        <small><?php echo wp_kses_post( $svalue['description']); ?></small>
                    <?php }
                    echo '<select class="control" name="'.$sid.'">';
                    foreach($svalue['choices'] as $choose_id => $choose_vl) { ?>
                        <option id="<?php echo $choose_id ?>" value="<?php echo $choose_id ?>" <?php if($the_theme_mod == $choose_id) echo 'selected' ?>/><?php echo $choose_vl ?></option>                                    
                <?php }
                    echo '</select>';
                break;

                //checkbox type
                case 'image': 
                    $image = $image_class = "";
                    $the_theme_mod_url = get_option_image($sid, false);
                    if($the_theme_mod){ 
                        $image = '<img src="'.$the_theme_mod_url.'"/>';
                        $image_class = ' hidden';
                    } ?>
        
                    <label class="label" for="<?php echo $sid ?>"><?php echo esc_html($svalue['name'] ); ?>:</label><br/>
                    <?php if ( isset( $svalue['description']) ) { ?>
                        <small><?php echo wp_kses_post( $svalue['description']); ?></small>
                    <?php } ?>
    
                    <div class="attachment-media-view">
                    
                        <div class="placeholder<?php echo esc_attr( $image_class ); ?>">
                            <?php esc_html_e( 'Chưa có ảnh nào được chọn', 'tw' ); ?>
                        </div>
                        <div class="thumbnail thumbnail-image">
                            <?php echo $image; ?>
                        </div>
        
                        <div class="actions clearfix">
                            <button type="button" class="btn mt-delete-button align-left"><?php _e( 'Xóa', 'twtheme' ); ?></button>
                            <button type="button" class="mt-upload-button btn alignright"><?php _e( 'Chọn Ảnh', 'twtheme' ); ?></button>
                            
                            <input name="<?php echo $sid; ?>" id="<?php $sid ?>" class="upload-id" type="hidden" value="<?php echo $the_theme_mod ?>" />
                        </div>
                    </div>
                <?php break;
            } ?>
        </li>
<?php }
}


function tw_option_show_about() { ?>
    <div class="about">
        <h1 align="center"><b><?php _e('3F Wordpress - 3F Group</b>.', 'twtheme') ?></b></h1>
        <p><?php _e('Cảm ơn bạn đã tin tưởng và lựa chọn dịch vụ cuả chúng tôi. Nếu trong quá trình sử dụng sản phẩm, quý khách gặp bất kỳ lỗi nào vui lòng thông báo cho chúng tôi qua cổng liên hệ <a href="https://3fgroup.vn/contact"><b>http://3fgroup.vn/contact</b></a> hoặc hotline <b>096 320 3338 - 0972915802</b>. Chúng tôi sẽ tư vấn và khắc phục lỗi nhanh nhất cho quý khách!', 'twtheme') ?></p>
        <p align="center"><?php _e('<b>Công ty CP phát triển nhân lực và công nghệ 3F Group</b><br/>Số 4 - Xóm Đình - Nguyên Xá - Bắc Từ Liêm - Hà Nội<br/>website: <a href="http://3fgroup.vn"><b>http://3fgroup.vn</b></a><br/>Hotline: <b>096 320 3338 - 097 291 5802</b>','twtheme') ?></p>
    </div>
<?php }
