<?php
/**
* the class WP_Options
* @author: Trieu Tai Niem
* @package: 3F Wordpress Core
* @link: https://3fgroup.vn
*/

$url_request = admin_url( 'admin.php?page=wp-theme-options');
//remove all options
if(isset($_GET['act']) && $_GET['act']=='clean'){
    remove_theme_mods();
    echo '<script>alert("Xóa các dữ liệu customizers thành công!");window.location.href="'.$url_request.'"</script>';
}

global $options;

$current_section_id = isset($_GET['section'])?$_GET['section']:key($options);

//current section for resquest
$current_section = isset($options[$current_section_id])?$options[$current_section_id]:array(); ?>

<div class="wrap">
    <h1 class="wp-heading-inline"><?php _e('Tùy Chỉnh Giao Diện', 'twtheme') ?></h1>
    <div class="options-content">
        <div class="sections">
            <ul>
                <li id="logo"><img style="height:45px;" src="<?php echo THEME_CORE_URL ?>/assets/images/logo_3f.png"></li>
            <?php
                $options['about'] = array('name'=> 'About');
                foreach($options as $section_id => $section) { 
                if($section_id===$current_section_id):?>
                    <li id="<?php echo $section_id ?>" class="active"><?php echo $section['name'] ?></li>
                <?php else: ?>
                    <li id="<?php echo $section_id ?>"><a href="<?php echo $url_request.'&section='.$section_id ?>"><?php echo $section['name'] ?></a></li>
            <?php endif; } ?>
                <div class="clear"></div>
            </ul>
        </div>
        <div class="options">
            <?php if($current_section_id=='about'):
                    tw_option_show_about();
            else: ?>
                <form id="options-form" action="" method="POST">
                    <div class="desc-action">
                        <?php if(isset($current_section['description'])) { ?><p class="desc"><i><?php echo $current_section['description'] ?></i></p> <?php } ?>
                        <button id="save-options" class="button button-primary button-large"><?php _e('Cập nhật', 'twtheme') ?></button>
                    </div>
                    <ul id="<?php echo $current_section_id ?>">
                        <?php tw_option_show_options($current_section) ?>
                    </ul>
                    <input type="hidden" value="<?php echo wp_create_nonce( 'twframework_nonce' ) ?>" name="twframework_nonce">
                </form>
            <?php endif ?>
        </div>
        <div class="clear"></div>
    </div>
</div>