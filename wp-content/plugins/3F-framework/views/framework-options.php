<?php
    $page_editor_blocked = (array)get_option('framework_remove-page-editor');
    $categories_blocked = (array)get_option('framework_block-categories');
    $pages_blocked = (array)get_option('framework_block-pages');

    if($_SERVER['REQUEST_METHOD']=='POST') {
        update_option('framework_remove-page-editor',$_POST['remove-page-editor']);
        update_option('framework_block-categories',$_POST['block-categories']);
        update_option('framework_block-pages',$_POST['block-pages']);

        echo '<script>window.location=window.location;</script>';
        die();
    }

    $allpages = get_posts(array(
        'post_type' => 'page',
        'posts_per_page' => -1
    ));

    $allcats = get_categories(array('hide_empty'=>false));
?>

<div class="wrap">
    <h1>Framework Options</h1>
    <form method="post" action="">
        <table class="form-table">
            <tbody>
                <tr class="option-site-visibility">
                    <th scope="row">Remove editor in page: </th>
                    <td>
                    <fieldset>
                        <select name="remove-page-editor[]" multiple>
                            <?php foreach($allpages as $p) { ?>
                                <option <?php echo in_array($p->ID, $page_editor_blocked)?'selected':'' ?> value="<?php echo $p->ID ?>"><?php echo $p->post_title ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>
                    </td>
                </tr>

                <tr class="option-site-visibility">
                    <th scope="row">Block Categories: </th>
                    <td>
                    <fieldset>
                        <select name="block-categories[]" multiple>
                            <?php foreach($allcats as $c) { ?>
                                <option <?php echo in_array($c->term_id, $categories_blocked)?'selected':'' ?> value="<?php echo $c->term_id ?>"><?php echo $c->name ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>
                    </td>
                </tr>

                <tr class="option-site-visibility">
                    <th scope="row">Block Pages: </th>
                    <td>
                    <fieldset>
                        <select name="block-pages[]" multiple>
                            <?php foreach($allpages as $p) { ?>
                                <option <?php echo in_array($p->ID, $pages_blocked)?'selected':'' ?> value="<?php echo $p->ID ?>"><?php echo $p->post_title ?></option>
                            <?php } ?>
                        </select>
                    </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Lưu thay đổi"></p>
        </form>
</div>