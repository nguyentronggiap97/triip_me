<?php 
/**
* The class help you make a widget easily
*
* @author: Trieu Tai Niem
* @link: https://facebook.com/trieuniem.it
*
* WARNING: This is a closed project, copyright by 3F Group.
* does not share this source code with anyone.
*
*/


abstract class TF_Widgets extends WP_Widget {

    public function __construct() {
        //get widget options
        $widget_ops  = $this->widget_defind();
        $widget_ops['classname'] = $widget_ops['widget_id'];
        parent::__construct($widget_ops['widget_id'] , $widget_ops['widget_name'] , $widget_ops );
    }

    //the asbstract method for defind TF widget
    abstract protected function widget_defind();

    //the asbstract method for create widget files
    abstract protected function widget_fields();

    //the asbstract method for show widget in to front
    abstract protected function widget_show( $args, $instance );


    public function widget( $args, $instance ) {
        $this->widget_show($args, $instance );
    }


    public function update( $new_instance, $old_instance ) {
        //method called when update widget
        $instance = $old_instance;

        $widget_fields = $this->widget_fields();

        // Loop through fields
        foreach ( $widget_fields as $widget_field ) {

            extract( $widget_field );

            //Use helper function to get updated field values
            $instance[$tw_widgets_name] = tw_widgets_updated_field_value( $widget_field, $new_instance[$tw_widgets_name] );
        }

        return $instance;
    }

    public function form( $instance ) {
        $widget_fields = $this->widget_fields();

        // Loop through fields
        foreach ( $widget_fields as $widget_field ) {

            // Make array elements available as variables
            extract( $widget_field );
            $tw_widgets_field_value = !empty( $instance[$tw_widgets_name] ) ? wp_kses_post( $instance[$tw_widgets_name] ) : '';
            tw_widgets_show_widget_field( $this, $widget_field, $tw_widgets_field_value );
        }
    }

}
