<?php 
/**
 * Archive file, the file show all post in a category
 * @author Trieu Tai Niem
 * @link http://3fgroup.vn
 */
?>

<?php get_header(); ?>

<?php get_sidebar() ?>
      
<?php if (have_posts()):

    while (have_posts()) : the_post();?> 
        <img src="<?php echo the_post_thumbnail_url('tw_thumbnail') ?>" alt="<?php the_title() ?>" />
        <h2><?php the_title() ?></h2>

        <p><?php the_excerpt(); ?></p>
    <?php endwhile;?> 

       <?php echo post_pagination(array(
        'container_class' => 'nav',
        'number_class' => 'pages',
        'current_class' => 'curr',
        'prev_text' => 'Trước',
        'next_text' => 'Sau',
    )) ?>
    
<?php else : ?>
    <h3><?php _e('Không có bài viết nào được tìm thấy.'); ?></h3>
<?php endif; ?>

<?php get_footer(); ?>