<?php 
/**
 * Comment file, the file show a post
 * called by comments_template() function
 * @author Trieu Tai Niem 
 * @link http://3fgroup.vn
 */
?>

<?php comment_form(); ?>
<ol class="commentlist">
    <?php wp_list_comments(); ?>
</ol>
