<?php 
/**
 * @author: Trieu Tai Niem
 * @package: 3F Web
 *
 * File to be add or expand widget area and functions related to the twtheme theme widget.
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */


add_action( 'widgets_init', function(){
    //rigister home widgets area
    register_sidebar( array(
        'name'          => esc_html__( 'Home Area', 'twtheme' ),
        'id'            => 'tw_home_area',
        'description'   => esc_html__( 'Thêm widget vào đây', 'twtheme' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    //rigister footer widgets area
    register_sidebar( array(
        'name'          => esc_html__( 'Footer Area', 'twtheme' ),
        'id'            => 'tw_footer_area',
        'description'   => esc_html__( 'Thêm widget cho phần chân trang web.', 'twtheme' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );

    //rigister side widgets area
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar Area', 'twtheme' ),
        'id'            => 'tw_sidebar',
        'description'   => esc_html__( 'Thêm widget cho phần sidebar bên trái trang web.', 'twtheme' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );
});


