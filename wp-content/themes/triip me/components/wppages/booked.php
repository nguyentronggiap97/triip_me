<?php
$data =get_option( 'booked');
?>
<div class="wrap">
	<table class="wp-list-table widefat fixed striped pages">
		<thead>
			<tr>
				<td style="width: 4%">
					STT
				</td>
				<td>
					Tên tour
				</td>
				<td>
					Họ và Tên
				</td>
				<td>
					Email
				</td>
				<td>
					Số ĐT
				</td>
				<td>
					Ngày bắt đầu
				</td>
				<td>
					Ngày Kết thúc
				</td>
				<td style="width: 7%">
					Số người
				</td>
				<td style="width: 7%">
					Số phòng
				</td>
				<td>
					Thời gian đặt
				</td>
				<td style="width: 5%">
					<input type="checkbox" id="check_all" name="">
				</td>
			</tr>
		</thead>
		<tbody>
			<?php 
			$stt =1;
			foreach ( (array) $data as $key => $value) {
			$post = get_post( $value['id_post'] );
			?>
				<tr id="key-<?php echo $key ?>">
					<td hidden ><input type="" name="id" id="id" value="<?php echo $key ?>"></td>
					<td><?php echo $stt ?></td>
					<td><a href="<?= get_permalink( $post->ID )?>"><?php echo $post->post_title; ?></a></td>
					<td><?php echo $value['name'] ?></td>
					<td><?php echo $value['email'] ?></td>
					<td><?php echo $value['field'] ?></td>
					<td><?php echo $value['start_day'] ?></td>
					<td><?php echo $value['end_day'] ?></td>
					<td><?php echo $value['people'] ?></td>
					<td><?php echo $value['room'] ?></td>
					<td><?php echo date( "Y-m-d H:i", ($key +((7)*3600)) )   ?></td>
					<td><input style="margin-left: 8px;" type="checkbox" id="<?php echo $key ?>" class="checkxxx" name=""></td>
				</tr>
			<?php	
			$stt++;
			} ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="11">
					<button class="button button-primary button-large btn_delete" style="float: right;" >Xóa những thông tin đã chọn</button>
					<div style="clear: both;"></div>
				</td>
			</tr>
		</tfoot>
		<script type="text/javascript">
				jQuery(document).ready(function($) {
					$('.btn_delete').click(function(event) {
						event.preventDefault();
						//var keyx=$(this).attr('id');
						var val = [];
						$('tbody :checkbox:checked').each(function(i){
							val[i] = $(this).attr('id');
						});
						console.log(val);
						console.log( JSON.stringify(val) );
						var res =JSON.stringify(val);

						var vld=confirm("Bạn chắc chắn muốn xóa chứ!");
						if (vld == true ) {
							$.ajax({
								url: '<?php echo admin_url( 'admin-ajax.php' ) ?>',
								type: 'POST',
								dataType: 'json',
								data: {
									key: val,
									action: 'ajax_delete_booked'
								 },
								success: function(data) {
									val.forEach(function(element) {
										$('#key-'+element).remove();
									});
									
									console.log(data);
								},
								error:function(data) {
									console.log("Thất bại: "+data);
								}
							});
						}
					});

					$('#check_all').click(function(event) {
						if (!check_all) {
							$( "tbody tr td .checkxxx" ).prop( "checked", false );
						}
						else{
							$( "tbody tr td .checkxxx" ).prop( "checked", true );
						}
						check_all=(!check_all);
					});
				});
			</script>
	</table>
</div>