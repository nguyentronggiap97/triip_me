<?php 

?>
<?php
$data =get_option( 'data_contact');
?>
<div class="wrap">
	<table class="wp-list-table widefat fixed striped pages">
		<thead>
			<tr>
				<td style="width: 4%">
					STT
				</td>
				<td style="width:15%">
					Họ và Tên
				</td>
				<td style="width: 16%">
					Email
				</td>
				<td style="width: 10%">
					Số ĐT
				</td>
				<td >
					Nội dung liên hệ
				</td>
				<td style="width: 12%">
					Thời gian đặt
				</td>
				<td style="width: 5%">
					<input type="checkbox" id="check_all" name="">
				</td>
			</tr>
		</thead>
		<tbody>
			<?php 
			$stt =1;
			foreach ( (array) $data as $key => $value) {
			$post = get_post( $value['id_post'] );
			?>
				<tr id="key-<?php echo $key ?>">
					<td hidden ><input type="" name="id" id="id" value="<?php echo $key ?>"></td>
					<td><?php echo $stt ?></td>
					<td><?php echo $value['name'] ?></td>
					<td><?php echo $value['email'] ?></td>
					<td><?php echo $value['phonenumber'] ?></td>
					<td><?php echo $value['content'] ?></td>
					<td><?php echo date( "Y-m-d H:i", ($key +((7)*3600)) )   ?></td>
					<td><input style="margin-left: 8px;" type="checkbox" id="<?php echo $key ?>" class="checkxxx" name=""></td>
				</tr>
			<?php	
			$stt++;
			} ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="7">
					<button class="button button-primary button-large btn_delete" style="float: right;" >Xóa những thông tin đã chọn</button>
					<div style="clear: both;"></div>
				</td>
			</tr>
		</tfoot>
		<script type="text/javascript">
				jQuery(document).ready(function($) {
					$('.btn_delete').click(function(event) {
						event.preventDefault();
						//var keyx=$(this).attr('id');
						var val = [];
						$('tbody :checkbox:checked').each(function(i){
							val[i] = $(this).attr('id');
						});
						console.log(val);
						console.log( JSON.stringify(val) );
						var res =JSON.stringify(val);

						var vld=confirm("Bạn chắc chắn muốn xóa chứ!");
						if (vld == true ) {
							$.ajax({
								url: '<?php echo admin_url( 'admin-ajax.php' ) ?>',
								type: 'POST',
								dataType: 'json',
								data: {
									key: val,
									action: 'ajax_delete_contact'
								 },
								success: function(data) {
									val.forEach(function(element) {
										$('#key-'+element).remove();
									});
									
									console.log(data);
								},
								error:function(data) {
									console.log("Thất bại: "+data);
								}
							});
						}
					});

					$('#check_all').click(function(event) {
						if (!check_all) {
							$( "tbody tr td .checkxxx" ).prop( "checked", false );
						}
						else{
							$( "tbody tr td .checkxxx" ).prop( "checked", true );
						}
						check_all=(!check_all);
					});
				});
			</script>
	</table>
</div>