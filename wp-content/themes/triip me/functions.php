<?php
/**
* @author: 3F Wordpress Team
* @project: 3F Wordpress
* @text: twtheme
* @link: http://3fgroup.vn
*
* Description: this is a primary file of WP theme
*/


//define the theme URL constant, use it to add public files such as js, css, imgaes, ect.
if(!defined('TFT_URL'))
    define( 'TFT_URL', get_template_directory_uri());
    
//define the theme PATH constant, use it to include or requied the PHP files.
if(!defined('TFT_PATH'))
    define( 'TFT_PATH', get_stylesheet_directory());

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
add_action( 'after_setup_theme',function() {
    /**
    * Make theme available for translation.
    * Translations can be filed in the /languages/ directory.
    * @link: https://codex.wordpress.org/Function_Reference/load_theme_textdomain
    */
    load_theme_textdomain( 'twtheme', get_template_directory() . '/languages' ); 


    /**  ADD THEME SUPPORT
     * Registers theme support for a given feature.
     * @link: https://developer.wordpress.org/reference/functions/add_theme_support/
     * 
     */

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
    * Let WordPress manage the document title.
    * By adding theme support, we declare that this theme does not use a
    * hard-coded <title> tag in the document head, and expect WordPress to
    * provide it for us.
    */
    add_theme_support( 'title-tag' );

    /**
     * Enable support for custom logo.
     */
    add_theme_support( 'custom-logo', array(
        'height'      => 50,
        'width'       => 240,
        'flex-height' => true,
        'flex-width' => true,       
    ) );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     */
    add_theme_support( 'post-thumbnails' );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    //<-------------------------END ADD THEME SUPORT-------------------->

    /**
     * Define custom image size
     * @link: https://developer.wordpress.org/reference/functions/add_image_size/
     */
    add_image_size( 'tw_thumbnail', 360, 230, true );
    add_image_size( 'tw_large', 1210, 642, true );

    //remove default images size
    add_filter('intermediate_image_sizes_advanced', function($sizes){
        unset( $sizes['thumbnail']);
        unset( $sizes['medium']);
        unset( $sizes['medium_large']);
        unset( $sizes['large']);
        return $sizes;
    });

    /**
    * Rigister side bar menus
    * This theme uses wp_nav_menu() in one location.
    * @link: https://codex.wordpress.org/Function_Reference/register_nav_menus
    */

    register_nav_menus( array(
        'tw-primary-menu' => esc_html__( 'Menu Chính', 'twtheme' ),
        'tw-footer-menu' => esc_html__( 'Menu Chân', 'twtheme' ),
        'tw-siderbar' => esc_html__( 'Menu Sidebar', 'twtheme' ),
    ));
});


//change more text in excerpt, default is "[...]"
add_filter( 'excerpt_more', function( $more ) {
    return '...';
});


if(function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title'    => 'Tùy Chỉnh Giao Diện',
        'menu_title'    => 'Tùy chỉnh',
        'position' => '59',
        'icon_url' => 'dashicons-admin-generic'
    ));
}

if(class_exists('WP_3FTheme')) {
    $include_files = array(
        WP_3FTheme::scan_dir(TFT_PATH.'/includes/hooks/', '/^[a-zA-Z-]{2,}.php$/'),
        WP_3FTheme::scan_dir(TFT_PATH.'/includes/functions/', '/^[a-zA-Z-]{2,}.php$/'),
        WP_3FTheme::scan_dir(TFT_PATH.'/includes/incfiles/', '/^[a-zA-Z-]{2,}.php$/'),
        WP_3FTheme::scan_dir(TFT_PATH.'/components/', '/^[a-zA-Z-]{2,}.php$/')
    ); 
    
    foreach($include_files as $files) {
        if($files == NULL)  continue;
        foreach($files as $f) {
            include $f;
        }
    }
}

// $lang =  mt_current_lg();

// add_action( 'rewrite_rules_array', 'rewrite_rules' );
// function rewrite_rules( $rules ) {
//     $new_rules = array();
//     foreach ( get_post_types() as $t )
//         $new_rules[ $t . '/([^/]+)\.html$' ] = 'index.php?post_type=' . $t . '&name=$matches[1]';
//     return $new_rules + $rules;
// }

// add_filter( 'post_type_link', 'custom_post_permalink' );
// function custom_post_permalink ( $post_link ) {
//     global $post;
//     if(empty($post)) return;
//     $type = get_post_type( $post->ID );
//     return home_url( $type . '/' . $post->post_name . '.html' );
// }

// add_filter( 'redirect_canonical', '__return_false' );