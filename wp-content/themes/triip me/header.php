<?php 
if (isset($_POST)) {
	if (isset($_POST['submit_singin'])) {
		
		$args['user_login'] = $_POST['email'];
		$args['user_password'] = $_POST['password'];
		$args['remember'] = $_POST['remember']=='on'?true:false;
		triip_login($args);
		?>
		<script type="text/javascript">
			window.location = window.location;
		</script>
		<?php
	}elseif(isset($_POST['submit_register'])) {
		$array_register['name'] = $_POST['firstname'].' '.$_POST['lastname'];
		$array_register['email'] = $_POST['email'];
		$array_register['password'] = $_POST['password'];
		$array_register['input-radio'] = $_POST['input-radio'];
		Register($array_register);
		?>
		<script type="text/javascript">
			alert("Bạn đã đăng ký thành công. Vui lòng đăng nhập để sử dụng!");
			window.location = "<?php echo home_url() ?>";
		</script>
		<?php

	}elseif (isset($_POST['submit_logout'])) {
		wp_logout();
		?>
		<script type="text/javascript">
			window.location = "<?php echo home_url() ?>";
		</script>
		<?php
	}elseif (isset($_POST['submitcontact'])) {
		send_from_contact($_POST);
		?>
		<script type="text/javascript">
			alert("Bạn đã gửi liên hệ thành công!");
			window.location = window.location;
		</script>
		<?php
	}
} ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		
		<meta charset="utf-8"/>
		
		<!-- <link rel="icon" type="image/x-icon" href="<?php  ?>" /> -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, shrink-to-fit=no, user-scalable=no"/>
		<meta name="keywords" content="coding, html, css"/>
		<!-- Styles-->
		<link rel="stylesheet" href="<?php echo TFT_URL ?>/public/libs/bootstrap/css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL ?>/public/libs/owlCarousel/assets/owl.theme.default.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL ?>/public/libs/owlCarousel/assets/owl.carousel.min.css"/>
		<link rel="stylesheet" href="<?php echo TFT_URL ?>/public/libs/Datepicker/bootstrap-datepicker.min.css"/>
		<link rel="stylesheet" href="<?php echo TFT_URL ?>/public/libs/fancybox-master/jquery.fancybox.min.css"/>
		<link rel="stylesheet" href="<?php echo TFT_URL ?>/public/libs/selectize/css/selectize.css"/>
		<link rel="stylesheet" href="<?php echo TFT_URL ?>/public/libs/selectize/css/selectize.default.css"/>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
		<link rel="stylesheet" href="//use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"/>
		<link rel="stylesheet" href="<?php echo TFT_URL ?>/public/libs/jquery-bar-rating-master/fontawesome-stars.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo TFT_URL ?>/public/style.css"/>
		<?php wp_head() ?>
	</head>
	<body>
		<div id="wrapper">
			<header id="header">
				<div class="container-fluid">
					<?php 
						$custom_logo_id = get_theme_mod( 'custom_logo' );
						$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
						
					?>
					<div class="row"><a class="logo-img float-left" style="background: url('<?php echo $image[0]; ?>');background-size: 48px;background-repeat: no-repeat;" href="<?php echo home_url() ?>"></a>
						<?php 
						if (!is_user_logged_in()) { ?>
						<nav class="right-header float-right">
							<ul class="list-inline">
								<li><a data-signin="data-signin" data-src="#popup-signin" href="javascript:;">Sign in</a></li>
								<li><a data-signin="data-signin" data-src="#popup-signin" href="javascript:;">register</a></li>
							</ul>
						</nav>
						<?php }else{ ?>
						<nav class="right-header float-right">
							<form method="post" action="">
								<ul class="list-inline">
									<li>
										<input class="logout-button" type="submit" name="submit_logout" value="Log out"/>
									</li>
								</ul>
							</form>
						</nav>
						<?php } ?>
						<div class="clear-fix"> </div>
					</div>
				</div>
			</header>
			
