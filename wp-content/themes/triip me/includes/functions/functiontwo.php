<?php 

add_action( 'wp_ajax_ajax_delete_contact','ajax_delete_contact' );
add_action( 'wp_ajax_nopriv_ajax_delete_contact','ajax_delete_contact' );
add_action( 'wp_ajax_ajax_delete_booked','ajax_delete_booked' );
add_action( 'wp_ajax_nopriv_ajax_delete_booked','ajax_delete_booked' );


function ajax_delete_contact(){
    $option_key="data_contact";
    $contact =get_option($option_key,false);
    $arr_check=( $_POST['key'] );
    foreach ($arr_check as  $value) {
        unset($contact[$value]);
    }
    if (empty($contact)) {
            delete_option('data_contact');
        }
    else{
        update_option( 'data_contact',$contact);
    }
    echo json_encode($contact);
    die();
}


function ajax_delete_booked(){
    $option_key="booked";
    $booked =get_option($option_key,false);
    $arr_check=( $_POST['key'] );
    foreach ($arr_check as  $value) {
        unset($booked[$value]);
    }
    if (empty($booked)) {
            delete_option('booked');
        }
    else{
        update_option( 'booked',$booked);
    }
    echo json_encode($booked);
    die();
}





 ?>



