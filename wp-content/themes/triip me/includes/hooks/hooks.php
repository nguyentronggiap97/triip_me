<?php 

add_action( 'triip_index_page', 'triip_index_banner',10 );
add_action( 'triip_index_page', 'triip_index_make_better',15 );
add_action( 'triip_index_page', 'triip_index_nice_place',20 );
add_action( 'triip_index_page', 'triip_index_member',25 );

function triip_index_banner(){ 
	$places = get_family_terms('places'); ?>
	<?php $banner =get_field('triip_banner',false);
	if(empty($banner)){
		$banner['url']= TFT_URL.'/public/images/banner.jpg';
	}
	?>

	<div class="banner" style="background-image: url('<?php echo $banner['url'] ?>');">
		<div class="banner-content">
			<div class="container-fluid">
				<h1 class="type-text"><p>
					<?php echo apply_filters('the_content', get_field('triip_home_title')) ?>
				</p></h1>
				<div class="date-input">
					<div class="container">
						<ul class="nav nav-tabs" role="tablist">
							<li class="active" role="presentation"><a href="#Book" aria-controls="home" role="tab" data-toggle="tab">Book A Stay</a></li>
							<li role="presentation"><a href="#Enjoy" aria-controls="profile" role="tab" data-toggle="tab">Enjoy Unique Experiences</a></li>
						</ul>
						<!-- Tab panes-->
						<div class="tab-content">
							<div class="tab-pane active" id="Book" role="tabpanel">
								<form action="<?php echo get_page_link(51) ?>" method="get">
									<div class="row">
										<div class="col-xs-12 col-lg-3 search-component">
											<div class="place-box">
												<div class="place-box__content">
													<div class="field">
														<div class="control-group text-left">
															<select class="demo-default" id="search-field" data-placeholder="Anywhere" name="search-field">
															<?php foreach ($places as $key_1 =>  $value): ?>
																<?php if (!empty($value['children'])): ?>
																	<?php foreach ($value['children'] as $key_2 => $keyx): ?>
																		<option value="<?php echo $key_2 ?>"><?php echo $keyx->name ?>, <?php echo $value['name'] ?></option>
																	<?php endforeach ?>
																<?php endif ?>
															<?php endforeach ?>
															</select>
														</div>
													</div>
												</div>
												<div class="over-play"></div>
											</div>
										</div>
										<div class="col-xs-12 col-lg-4 search-component">
											<div class="date-range-picker">
												<div class="DateRangePicker">
													<div class="DateRangePickerInput">
														<div class="DateInput float-left"> 
															<div class="form-group">
																<div class="filterDate">
																	<!-- Datepicker as text field-->
																	<div class="input-group date" data-date-format="dd.mm.yyyy">
																		<input class="form-control" name="start_date" type="text" placeholder="Start Day"/>
																		<div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div>
																	</div>
																</div>
															</div>
														</div>
														<div class="Date-arrow float-left"><i class="fas fa-long-arrow-alt-right"></i></div>
														<div class="DateInput float-right">
															<div class="form-group">
																<div class="filterDate">
																	<!-- Datepicker as text field-->
																	<div class="input-group date" data-date-format="dd.mm.yyyy">
																		<input class="form-control text-right" name="end_date" type="text" placeholder="End Day"/>
																		<div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div>
																	</div>
																</div>
															</div>
														</div>
														<div class="clear-fix"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-lg-3 search-component">
											<div class="participant col-sm-12">
												<div class="participant__content">
													<input type="text" name="people"/>
													<div class="field">
														<button class="participant-button"><i class="fas fa-users"></i>1 guest · 1 room</button>
													</div>
													<ul class="participant__list">
														<li class="room">
															<button class="pre">-</button><span>1</span>
															<button class="add">+</button>
														</li>
														<li class="adults">
															<button class="pre">-</button><span>1</span>
															<button class="add">+</button>
														</li>
														<li class="children">
															<button class="pre">-</button><span>0</span>
															<button class="add">+</button>
														</li>
													</ul>
												</div>
												<div class="over-play"></div>
											</div>
										</div>
										<div class="col-xs-12 col-lg-2 search-component">
											<button class="search-button" type="submit">Let's Go</button>
											<!-- <input type="submit" class="search-button" value="Let's go" name="submit_1"> -->
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane" id="Enjoy" role="tabpanel">
								<div class="row">
									<form action="<?php echo home_url() ?>">
										<div class="col-xs-12 col-lg-9 search-component">
											<div class="search-box">
												<div class="control-group text-left">
													<input class="demo-default" type="text" name="s" data-placeholder="location"/>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-lg-3 search-component">
											<button class="search-button" type="submit">Let's Go</button>
										</div>
										<div class="clear-fix"></div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}


function triip_index_make_better(){ 
$list = (array)get_field('triip_list',21,'option');
// echo '<pre>'.__FILE__ .'::'.__METHOD__ .'('.__LINE__ .')<br>';
// 	print_r($list);
// echo '</pre>';
?>
	<section class="make-better">
		<div class="make-better">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-lg-10 col-lg-offset-1">
						<div class="row text-center">
							<div class="col-xs-12">
								<div class="make-better__title">
									<h1>WHAT MAKES US BETTER?</h1>
								</div>
							</div>
						</div>
						<div class="row">
						<?php foreach ($list as  $value): ?>
							<div class="col-sm-4">
								<article>
									<div class="cube price" style="background-image: url(<?php echo $value['background']['url'] ?>);">
										<img src="<?php echo $value['icon']['url'] ?>" alt=""/>
										<div class="cube__content">
											<h4 class="cube__title"><?php echo $value['title'] ?></h4>
											<p class="cube__text">
											<?php foreach ($value['list_strength'] as $key): ?>
												<?php echo $key['name'] ?><br/>
											<?php endforeach ?></p>
										</div>
									</div>
								</article>
							</div>
						<?php endforeach ?>
							
							<div class="col-sm-4">
								<article>
									<div class="cube tour-free"><img src="<?php echo TFT_URL ?>/public/images/cube-icon2.svg" alt=""/>
										<div class="cube__content">
											<h4 class="cube__title">Price Beat</h4>
											<p class="cube__text">Best price<br/>automagically<br/>guaranteed</p>
										</div>
									</div>
								</article>
							</div>
							<div class="col-sm-4">
								<article>
									<div class="cube match"><img src="<?php echo TFT_URL ?>/public/images/cube-icon3.svg" alt=""/>
										<div class="cube__content">
											<h4 class="cube__title">Price Beat</h4>
											<p class="cube__text">Best price<br/>automagically<br/>guaranteed</p>
										</div>
									</div>
								</article>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 text-center">
								<button class="btn btn-primar btn-lg cube-active">ready to go?</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
}


function triip_index_nice_place(){

$terms = get_terms( array(
	'taxonomy' => 'places',
	'hide_empty' => false,
) );
// echo '<pre>'.__FILE__.'::'.__METHOD__.'('.__LINE__.')<br>';
// 	print_r($terms);
// echo '</pre>'; 
?>
	<section class="nice-places">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 text-center">
					<h1 class="nice-places__title">Travelers Love These Places</h1>
					<h3 class="nice-places__sub-title">Thousand cities in <?php echo count(get_family_terms('places')) ?> countries, with our unique local experiences</h3>
					<div class="row">
						<?php $count = 1; ?>
						<?php foreach ($terms as $key => $value): ?>
						<?php 
							if ($count==13) {
								break;
							}
							$img = get_field('triip_thumbnail_places',$value->taxonomy . '_' . $value->term_id);
							if(empty($img)){
								$img['url']=TFT_URL.'/public/images/place-rectangle1.jpg';
							}
							$count++;
						?>
							<div class="col-xs-12 col-sm-6 col-lg-3 place">
								<a href="<?php echo get_term_link( $value ); ?>">
									<div class="place-content" style="background-image: url('<?php echo $img['url'] ?>')">
										<h6><?php echo $value->name ?></h6>
									</div>
								</a>
							</div>
						<?php endforeach ?>
					</div>
					<div class="row">
						<div class="col-xs-12 text-center">
							<button class="btn btn-primar btn-lg cube-active">Book Your Experience Now!</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
}


function triip_index_member(){
?>
	<section class="member">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2 text-center">
					<h1 class="member__title">Travelers Love These Places</h1>
					<div class="row">
						<div class="col-xs-12 col-sm-4 text-center">
							<div class="member__images"><img src="<?php echo TFT_URL ?>/public/images/become-cube1.svg" alt=""/></div>
							<h4 class="member__heading">Cashback</h4>
						</div>
						<div class="col-xs-12 col-sm-4 text-center">
							<div class="member__images"><img src="<?php echo TFT_URL ?>/public/images/become-cube2.svg" alt=""/></div>
							<h4 class="member__heading">Free tour</h4>
						</div>
						<div class="col-xs-12 col-sm-4 text-center">
							<div class="member__images"><img src="<?php echo TFT_URL ?>/public/images/become-cube3.svg" alt=""/></div>
							<h4 class="member__heading">bottle</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 text-center">
							<button class="btn btn-primar btn-lg cube-active">Become A Member Now!</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
}

