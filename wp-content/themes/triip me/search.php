<?php 
/*
*Template Name: smart search Page
*/
?>
<?php get_header(  ) ?>
<?php 
	global $wp_query, $wp;
	$link = home_url( $wp->request );
	if (isset($_GET['order'])) {
		if ($_GET['order']=='review') {
			$meta_key = 'triip_review_score';
		}elseif ($_GET['order']=='popularity') {
			$meta_key = 'count_view';
		}else{
			$meta_key = 'ranker';
		}
	}else{
		$_GET['order']= 'triip_ranker';
		$meta_key =$_GET['order'];
	}

	$link .='?order='.$meta_key.'&pg=';

	$args = array(
		// 'numberposts'	=> 1,
		'posts_per_page' => 5,
		'post_type'		=> 'the-tours',
		'meta_key' => $meta_key,
	    'orderby' => 'meta_value_num',
	    'order' => 'DESC',
	    'paged' => isset($_GET['pg'])?$_GET['pg']:1,
	    's'=> get_search_query( )
	);
	$the_query = new WP_Query( $args ); 
	if ($the_query->have_posts()) {
		
		$classx = '<i class="fas fa-sort-amount-down" ></i>&nbsp;';
		$term =get_term( $wp_query->queried_object_id,'places' );
		$total = ($the_query->max_num_pages)*$args['posts_per_page'];
	?>
		<div class="page2-content">
			<div class="container">
				<div class="row">
					<div class="col-sm-12"> 
						<div class="page2-content__title">
							<!-- <h3><?php echo $term->name ?> (<?php echo count($posts) ?>)</h3> -->
						</div>
						<div class="page2-content__menu">
							<ul class="list-inline">
								<li>
									<a href="?order=ranking">
										<?php if ($meta_key=='ranker') {
											echo $classx;
										} ?>
										Ranking
									</a>
								</li>
								<li>
									<a  href="?order=popularity">
										 <?php if ($meta_key=='count_view') {
										 	echo $classx;
										} ?>
										Popularity
									</a>
								</li>
								<li>
									<a href="?order=review">
										<?php if ($meta_key=='triip_review_score') {
											echo $classx;
										} ?>
										Review Score
									</a>
								</li>
							</ul>
						</div>
						<div class="page2-content__list-item">
					<?php
					while ( $the_query->have_posts() ) : $the_query->the_post(); 
						$count_view = get_post_meta( $post->ID, 'count_view',true);
						$triip_location =get_field('triip_location',false);
						$triip_review_score =get_field('triip_review_score',false);
						?>
							<div class="item-content">
								<div class="row">
									<div class="col-sm-6">
										<div class="item-img"><a href="<?php echo get_permalink( $post ) ?>"><img src="<?php echo get_the_post_thumbnail_url( $post, 'full' ) ?>" alt=""/></a></div>
									</div>
									<?php 
									$rank =get_post_meta( $post->ID,'ranker',true );
									$tong=0;
									if (!empty($rank)) {
										foreach ($rank as $value) {
											@$tong +=$value;
										}
										if ($tong>0) {
											$rank_star = $tong/count($rank);
										}
									}else{
										$rank_star=1;
									}
									
									$rank_star = !$rank_star?'1':$rank_star;
									?>
									<div class="col-sm-6">
										<div class="item-text">
											<h4><a href="<?php echo get_permalink( $post ) ?>"><?php echo the_title( ) ?></a></h4>
											<ul class="list-inline">
											<?php for ($i=0; $i < $rank_star ; $i++) { ?>
												<li><i class="fas fa-star"></i></li>
											<?php } ?>
											</ul>
											<p><i class="fas fa-map-marker-alt fleft"></i><span class="fleft"><?php echo $triip_location ?></span>
												<div class="clear-fix"></div>
											</p>

											<p class="views"><i class="fas fa-eye"></i><span><?php echo $count_view ?></span></p>
											<p class="reviewscore-wrap"><span class="reviewscore">Review score</span><span class="reviewscore-value"><?php echo $triip_review_score ?></span></p>
											

											<div class="item-btn"><a class="btn btn-primary" href="<?php echo get_permalink( $post ) ?>">View Rooms</a></div>
										</div>
									</div>
								</div>
								<div class="hotel-card">
									<p>Extra Cashback</p>
								</div>
							</div>
						<?php endwhile;?>
						</div>
						<div class="page2-content__page-number">
							<?php mt_destination_navigate(false,$the_query,$link); ?>
							<?php //wp_corenavi_table($wp_query) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	}
	else{
		?><h2 style="margin: 100px auto 50px; width: 100%; text-align: center;">No tours found!</h2><?php
	}

?>
<?php get_footer(  ) ?>