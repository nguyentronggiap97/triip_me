<?php session_start(); 
get_header( ); ?>
<?php 
while(have_posts()) {
 	the_post(); 
 	$id_author = $post->post_author;
 	$taxo_child =get_the_terms( $post, 'places' );
 	$id_taxo_parent = $taxo_child[0]->parent;
 	$taxo_parent =get_term( $id_taxo_parent, 'places');
 	$categories = get_the_terms( $post, 'the-categories' );
	$summary = get_field('triip_summary',false); //Tóm tắt
	$duration = get_field('triip_duration',false);//Thời lượng
	$departure = get_field('triip_departure',false);//Khởi hành
	$price_per_participant = get_field('triip_price_per_participant',false);//Giá cho mỗi thành viên
	$languages = get_field('triip_languages',false);//Ngôn ngữ
	$group_size = get_field('triip_group_size',false);//Số người tham gia
	$transportation = get_field('triip_transportation',false);//Phương tiên vận chuyển
	$include_tour = get_field('triip_include_tour',false);//Dịch vụ của tour
	$exclude_tour = get_field('triip_exclude_tour',false);//Những dịch vụ không có
	$cancellation = get_field('triip_cancellation',false);//Hủy bỏ
	$triip_banner = get_field('triip_banner',false);
	//anti-spam
	if (!isset($_SESSION['id-'.$post->ID])) {
		$_SESSION['id-'.$post->ID] = time();
		add_post_meta( $post->ID, 'count_view',1 );
	}else{
		count_view($post->ID,$_SESSION['id-'.$post->ID]);
	}
	//coment
	if (isset($_POST['btn_comment'])) {
		$comment = $_POST['txt_comment'];
		$current_user = wp_get_current_user();
		$time = current_time('mysql');
		$data = array(
			'comment_post_ID' => get_the_ID(),
			'comment_author' => $current_user->data->display_name,
			'comment_author_email' => $current_user->data->user_email,
			'comment_content' => $comment,
			'user_id' => $current_user->ID,
			'comment_date' => $time,
			'comment_approved' => 1,
			'comment_type' => 'custom-comment-class'
		);
		wp_insert_comment($data);
		global $wp;

		echo ' <script type="text/javascript"> window.location="'.$wp->request.'"; </script> ';
		die();
	}
	//submit star
	if (isset($_POST['rank_submit'])) {
		$current_user = wp_get_current_user();
		add_ranker(get_the_ID(),$current_user->ID,$_POST['voting']);
	}
	//Book tour
	if(isset($_POST['submit_book'])){
		if (is_user_logged_in()) {
			$option_key="booked";
			$arr['name'] = $_POST['name'];
			$arr['email'] = $_POST['email'];
			$arr['field'] = $_POST['field'];
			$arr['start_day'] = $_POST['start_day'];
			$arr['end_day'] = $_POST['end_day'];
			$arr['people'] = $_POST['people'];
			$arr['room'] = $_POST['room'];
			$arr['id_post'] = $_POST['id_post'];
		    
		    $contact =get_option($option_key,false);
		    if (!$contact) {
		        $contact[time()] = $arr;
		        add_option( 'booked',$contact );
		    }
		    else{
		        $contact[time()] = $arr;
		        update_option( 'booked',$contact);    
		    }
		    global $wp;

			echo ' <script type="text/javascript"> window.location="'.$wp->request.'"; </script> ';
			die();
		}else{
		?>
			<script type="text/javascript">
				alert("Bạn cần đăng nhập để đặt tour!");
			</script>
		<?php
		}
	}

	?>
	
<div class="single-banner">
	<div class="single-banner__img" style="background: url('<?php echo $triip_banner['url'] ?>');"></div>
</div>
<div class="header-fixed">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-8">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-4">
						<nav class="menu-fixed">
							<ul class="list-inline">
								<li class="Special"><a href="#">Special</a></li>
								<li class="Information"><a href="#">Information</a></li>
								<li class="Itinerary"><a href="#">Itinerary</a></li>
								<li class="Reviews"><a href="#">Reviews</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="main single-page">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8">
				<section class="info row">
					<div class="col-lg-4 col-md-3 text-center">
						<div class="info-author"><a class="info-author__img" href=""><img src="<?php echo get_avatar_url( $id_author ) ?>" alt=""/></a>
							<h3 class="info-author__name"><a href="#"><?php echo get_author_name( $id_author ) ?></a></h3>
							<div class="info-author__contact"><a href="#">talk to me</a></div>
						</div>
					</div>
					<div class="col-lg-8 col-md-9">
						<div class="info-content">
							<div class="info-content__title">
								<h2><?php the_title() ?></h2>
							</div>
							<div class="info-content__sub"><span class="location-icon"></span><a href="#"><?php echo $taxo_parent->name ?></a>, <a href="<?php echo get_term_link( $taxo_child[0], 'places' ) ?>"><?php echo $taxo_child[0]->name ?></a></div>
							<div class="row">
								<div class="col-xs-12">
									<?php echo $summary; ?>
								</div>
							</div>
						</div>
					</div>
				</section>
				<setion class="tour-information">
					<div class="row">
						<h3 class="tour-information__title">Tour Information</h3>
						<div class="col-lg-6 col-md-6 tour-information__info left">
							<div class="row item">
								<div class="col-xs-4 name">
									<p>Location</p>
								</div>
								<div class="col-xs-8 place text-right"><a href="<?php echo get_term_link( $taxo_child[0], 'places' ) ?>"><?php echo $taxo_child[0]->name ?></a></div>
							</div>
							<div class="row item">
								<div class="col-xs-4 name">
									<p>Duration </p>
								</div>
								<div class="col-xs-8 place text-right">
									<p><?php echo $duration; ?> Ngày</p>
								</div>
							</div>
							<div class="row item">
								<div class="col-xs-4 name">
									<p>Departure  </p>
								</div>
								<div class="col-xs-8 place text-right">
									<p><?php echo $departure; ?></p>
								</div>
							</div>
							<div class="row item">
								<div class="col-xs-4 name">
									<p>Price per Participant</p>
								</div>
								<div class="col-xs-8 place text-right">
									<p>$ <?php echo $price_per_participant; ?></p>
								</div>
							</div>
							<div class="row item">
								<div class="col-xs-4 name">
									<p>Languages</p>
								</div>
								<div class="col-xs-8 place text-right">
									<p><?php echo $languages; ?></p>
								</div>
							</div>
							<div class="row item">
								<div class="col-xs-4 name">
									<p>Group size </p>
								</div>
								<div class="col-xs-8 place text-right">
									<p><?php echo $group_size; ?> Người</p>
								</div>
							</div>
							<div class="row item">
								<div class="col-xs-4 name">
									<p>Categories</p>
								</div>
								<div class="col-xs-8 place text-right">
									<?php $count=1 ;foreach ($categories as $key => $value): ?>
										<a href="<?php echo get_term_link( $value, 'the-categories' ) ?>"><?php echo $value->name ?></a>
										<?php if ($count!=count($categories)): ?>
												<?php echo ',' ?>
										<?php endif;
										$count++;
										endforeach ?>
									
								</div>
							</div>
							<div class="row item">
								<div class="col-xs-4 name">
									<p>Transportation</p>
								</div>
								<div class="col-xs-8 place text-right">
									<p><?php echo $transportation; ?></p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 tour-information__info right">
							<div class="row item">
								<div class="name col-xs-4">
									<p>Includes </p>
								</div>
								<div class="col-xs-8 place">
									<p><?php echo $include_tour; ?></p>
								</div>
							</div>
							<div class="row item">
								<div class="col-xs-4 name">
									<p>Excludes </p>
								</div>
								<div class="col-xs-8 place">
									<p><?php echo $exclude_tour; ?></p>
								</div>
							</div>
							<div class="row item">
								<div class="col-xs-4 name">
									<p>Cancellation</p>
								</div>
								<div class="col-xs-8 place">
									<p><?php echo $cancellation; ?></p>
								</div>
							</div>
						</div>
					</div>
				</setion>
				<section class="itinerary">
					<article class="step">
						<?php the_content() ?>
					</article>
					
				</section>
				<section class="review">
					<h3 class="review__title">Review</h3>
					<?php 
					$args = array(
						'post_id' => get_the_ID(),
						'orderby' => 'date',
						'order' => 'DESC',
					);
					$list_infor_comments=get_comments( $args );
					$rank =get_post_meta( get_the_ID(),'ranker',true );
					$tong=0;
					if (!empty($rank)) {
						foreach ($rank as $value) {
							@$tong +=$value;
						}
						if ($tong>0) {
							$rank_star = $tong/count($rank);
						}
					}else{
						$rank_star=1;
					}
					$rank_star = !$rank_star?'1':$rank_star;
					?>
					<div class="review-list">
						<?php foreach ($list_infor_comments as $key => $value): ?>
							<div class="review-wrap">
								<figure class="fleft"><img src="<?php echo get_avatar_url( $value->user_id, array(150,150) ) ?>" alt=""/></figure>
								<main class="fleft"><span><?php echo $value->comment_author ?></span>
									<ul class="list-inline">
									<?php if(array_key_exists($value->user_id, $rank)==true){ ?>
										<?php for ($i=0; $i < $rank[$value->user_id] ; $i++) { ?>
											<li><i class="fas fa-star"></i></li>
										<?php } ?>
									<?php } ?>
									</ul>
									<p><?php echo $value->comment_content ?></p>
									<date><small><i><?php echo date("Y - m -d  H:i", strtotime($value->comment_date)) ?></i></small></date>
								</main>
								<div class="clear-fix"></div>
							</div>
						<?php endforeach ?>
					</div>
					<div class="leavecomment">
						<h3 class="review__title">Comment</h3>
						
						<form id="votingform" action="" method="post">
							<select id="votingbar" name="voting">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
							<input type="text" name="getValStar" hidden value="<?php echo $rank_star ?>"/>
							<button name="rank_submit">submit your vote</button>
						</form>
						<form id="comment" action="" method="post">
							<textarea id="comment" name="txt_comment" cols="30" rows="10" placeholder="Leave your comment here ..."></textarea>
							<button name="btn_comment" >submit comment</button>
						</form>
					</div>
				</section>
				
				<section class="how-work">
					<div class="row">
						<div class="col-xs-12 text-center"><img src="images/how-work.png" alt=""/></div>
					</div>
				</section>
			</div>

			<div class="col-lg-4 col-md-4">
				<div class="book"><i class="fas fa-times"></i>
					<div class="price-info row">
						<div class="col-xs-12"><span class="price-info__number">$120<span>/ TIIM 1333.20<i class="fas fa-info-circle"></i></span></span></div>
					</div>
					<form class="cmxform" id="booking-form" method="post" action="">
						<fieldset>
							<p>
								<label for="cname">Name:</label>
								<input id="cname" name="name" minlength="2" type="text" required=""/>
							</p>
							<p>
								<label for="cemail">E-Mail:</label>
								<input id="cemail" type="email" name="email" required=""/>
							</p>
							<p>
								<label for="field">Number: </label>
								<input class="left" id="field" name="field"/>
							</p>
							<div class="date-range-picker">
								<div class="DateRangePicker">
									<div class="DateRangePickerInput">
										<div class="DateInput float-left"> 
											<div class="form-group">
												<div class="filterDate">
													<!-- Datepicker as text field-->
													<div class="input-group date" data-date-format="dd.mm.yyyy">
														<input class="form-control" type="text" name="start_day" placeholder="Start Day"/>
														<div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div>
													</div>
												</div>
											</div>
										</div>
										<div class="Date-arrow float-left"><i class="fas fa-long-arrow-alt-right"></i></div>
										<div class="DateInput float-right">
											<div class="form-group">
												<div class="filterDate">
													<!-- Datepicker as text field-->
													<div class="input-group date" data-date-format="dd.mm.yyyy">
														<input class="form-control text-right" name="end_day" type="text" placeholder="End Day"/>
														<div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div>
													</div>
												</div>
											</div>
										</div>
										<div class="clear-fix"></div>
									</div>
								</div>
							</div>
							<div class="search-component row">
								<div class="participant col-sm-12">
									<div class="participant__content">
										<input type="text" name="people"/>
										<input type="text" style="display: none;" name="room"/>
										<div class="field">
											<button class="participant-button"><i class="fas fa-users"></i>1 guest · 1 room</button>
										</div>
										<ul class="participant__list">
											<li class="room">
												<button class="pre">-</button><span>1</span>
												<button class="add">+</button>
											</li>
											<li class="adults">
												<button class="pre">-</button><span>1</span>
												<button class="add">+</button>
											</li>
											<li class="children">
												<button class="pre">-</button><span>0</span>
												<button class="add">+</button>
											</li>
										</ul>
									</div>
									<div class="over-play"></div>
								</div>
								<input style="display: none;" type="text" name="id_post" value="<?php echo $post->ID ?>">
							</div>
							<p class="text-center">
								<input class="submit" type="submit" name="submit_book" value="Request this triip"/>
							</p>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<span class="btn-requestmobile" style="display: block;">REQUEST THIS TRIP!</span>
</section>

<?php } ?>
<?php get_footer(  ) ?>