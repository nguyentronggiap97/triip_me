<?php 
/**
 * Page file, the file show a single page
 * @author Trieu Tai Niem 
 * @link http://3fgroup.vn
 */
?>


<?php get_header(); ?>

<?php get_sidebar() ?>
      
<?php if (have_posts()):

    while (have_posts()) : the_post();?> 
        <?php the_post_thumbnail( 'full' ) ?>
        <h2><?php the_title() ?></h2>

        <p><?php the_content(); ?></p>

        <?php comments_template(); ?>

<?php endwhile; endif; ?> 

<?php get_footer(); ?>