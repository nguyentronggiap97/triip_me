<?php 

/* 
    Template Name: 3F Contact Page
*/

//add ajax request
get_header();
?>
<section id="contactus">
	<div class="container">
		<h2 class="title">Contact us</h2>
		<form id="contact-form" action="" method="post">
			<div class="input-wrap">
				<div class="label-wrap">
					<label for="name">Name:</label>
				</div>
				<div class="input">
					<input id="name" type="text" name="name" placeholder="Your name here"/>
				</div>
				<div class="clear-fix"></div>
			</div>
			<div class="input-wrap">
				<div class="label-wrap">
					<label for="phone">Phone Number:</label>
				</div>
				<div class="input">
					<input id="phone" type="text" name="phonenumber" placeholder="Phone Number"/>
				</div>
				<div class="clear-fix"></div>
			</div>
			<div class="input-wrap">
				<div class="label-wrap">
					<label for="email" placeholder="E-mail">E-mail: </label>
				</div>
				<div class="input">
					<input id="email" type="email" name="email" placeholder="E-mail"/>
				</div>
				<div class="clear-fix"></div>
			</div>
			<div class="input-wrap">
				<div class="label-wrap">
					<label for="textarea" >Content: </label>
				</div>
				<div class="input">
					<textarea id="content" name="content" cols="30" rows="10" placeholder="Content"></textarea>
				</div>
				<div class="clear-fix"></div>
			</div>
			<div class="input-wrap submit-button">
				<center>
					<!-- <input type="submit" name="submitcontact" value="ahihi"> -->
					<button type="submit" name="submitcontact">Submit</button>
				</center>
			</div>
		</form>
	</div>
</section>

<?php
get_footer(  );