<?php 
/*
*Template Name: smart search Page
*/
?>
<?php get_header(  ) ?>
<?php 

	$_GET['start_date'] =str_replace( '/', '.',$_GET['start_date']);// str_replace( '/', '.', '23/08/2018' );
	$_GET['end_date'] = str_replace( '/', '.',$_GET['end_date']);
	$days = (strtotime(str_replace( '/', '.',$_GET['end_date']))-strtotime(str_replace('/', '.',$_GET['start_date'])))/86400;
	if (isset($_GET['order'])) {
		if ($_GET['order']=='review') {
			$meta_key = 'triip_review_score';
		}elseif ($_GET['order']=='popularity') {
			$meta_key = 'count_view';
		}else{
			$meta_key = 'ranker';
		}
	}else{
		$_GET['order']= 'triip_ranker';
		$meta_key =$_GET['order'];
	}
	$args = array(
		// 'numberposts'	=> 1,
		'posts_per_page' => -1,
		'post_type'		=> 'the-tours',
		'meta_key' => $meta_key,
	    'orderby' => 'meta_value_num',
	    'order' => 'DESC',
		'meta_query'	=> array(
			'relation'		=> 'AND',
			array(
				'key'		=> 'triip_departure',
				'compare'	=> '<',
				'value'		=> str_replace( '.', '/',$_GET['start_date']),
			),
			array(
				'key'		=> 'triip_duration',
				'compare'	=> '<=',
				'type'		=> 'NUMERIC',
				'value'		=> $days,
			),
			array(
				'key'		=> 'triip_group_size',
				'compare'	=> '>=',
				'type'		=> 'NUMERIC',
				'value'		=> $_GET['people'],
			),
		),
		'tax_query' => array( //'relation' => 'OR', 
			array( 
				'taxonomy' => 'places', 
				'flied' => 'term_id', 
				'terms' =>$_GET['search-field'], 
				//'operator' => 'IN' 
			),
		) 
	);
	$the_query = new WP_Query( $args ); 
	if ($the_query->have_posts()) {
		$countxx =1;
		$url_get = '';
		foreach ($_GET as $key => $value) {
			$url_get .=$key.'='.$value.'&';
			if ($countxx==(count($_GET)-1)) {
				break;
			}
			$countxx++;
		}
		$classx = '<i class="fas fa-sort-amount-down" ></i>&nbsp;';
		$term =get_term( $_GET['search-field'],'places' );
	?>
		<div class="page2-content">
			<div class="container">
				<div class="row">
					<div class="col-sm-12"> 
						<div class="page2-content__title">
							<h3><?php echo $term->name ?> (<?php echo count($the_query->posts) ?>)</h3>
						</div>
						<div class="page2-content__menu">
							<ul class="list-inline">
								<li>
									<a href="?<?php echo $url_get ?>order=ranking">
										<?php if ($meta_key=='ranker') {
											echo $classx;
										} ?>
										Ranking
									</a>
								</li>
								<li>
									<a  href="?<?php echo $url_get ?>order=popularity">
										 <?php if ($meta_key=='count_view') {
										 	echo $classx;
										} ?>
										popularity
									</a>
								</li>
								<li>
									<a href="?<?php echo $url_get ?>order=review">
										<?php if ($meta_key=='triip_review_score') {
											echo $classx;
										} ?>
										Review Score
									</a>
								</li>
							</ul>
						</div>
						<div class="page2-content__list-item">
					<?php
					while ( $the_query->have_posts() ) : $the_query->the_post(); 
						$count_view = get_post_meta( $post->ID, 'count_view',true);
						$triip_location =get_field('triip_location',false);
						$triip_review_score =get_field('triip_review_score',false);
						?>

							<div class="item-content">
								<div class="row">
									<div class="col-sm-6">
										<div class="item-img"><a href="<?php echo get_permalink( $post ) ?>"><img src="<?php echo get_the_post_thumbnail_url( $post, 'full' ) ?>" alt=""/></a></div>
									</div>
									<?php 
									$rank =get_post_meta( $post->ID,'ranker',true );
									$tong=0;
									if (!empty($rank)) {
										foreach ($rank as $value) {
											@$tong +=$value;
										}
										if ($tong>0) {
											$rank_star = $tong/count($rank);
										}
									}else{
										$rank_star=1;
									}
									
									$rank_star = !$rank_star?'1':$rank_star;
									?>
									<div class="col-sm-6">
										<div class="item-text">
											<h4><a href="<?php echo get_permalink( $post ) ?>"><?php echo the_title( ) ?></a></h4>
											<ul class="list-inline">
												<li><i class="fas fa-star"></i></li>
												<li><i class="fas fa-star"></i></li>
												<li><i class="fas fa-star"></i></li>
												<li><i class="fas fa-star"></i></li>
											</ul>
											<p><i class="fas fa-map-marker-alt fleft"></i><span class="fleft"><?php echo $triip_location ?></span>
												<div class="clear-fix"></div>
											</p>

											<p class="views"><i class="fas fa-eye"></i><span><?php echo $count_view ?></span></p>
											<p class="reviewscore-wrap"><span class="reviewscore">Review score</span><span class="reviewscore-value"><?php echo $triip_review_score ?></span></p>
											
											<div class="item-btn"><a class="btn btn-primary" href="<?php echo get_permalink( $post ) ?>">View Rooms</a></div>
										</div>
									</div>
								</div>
								<div class="hotel-card">
									<p>Extra Cashback</p>
								</div>
							</div>

						<?php endwhile;?>
						</div>
						<div class="page2-content__page-number">
							<?php //mt_destination_navigate(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	}
	else{
		?><h2 style="margin: 100px auto 50px; width: 100%; text-align: center;">No tours found!</h2><?php
	}

?>
<?php get_footer(  ) ?>