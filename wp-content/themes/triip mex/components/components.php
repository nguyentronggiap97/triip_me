<?php
/**
* @author: 3F Wordpress Team
* @project: 3F Wordpress
* @textdomain: twtheme
* @link: http://3fgroup.vn
*
* Description: this is a primary file of WP theme
* @global: $twtheme - Object WP_Themes
*/

global $twtheme;
if(!is_object($twtheme)) return;


//ajax chạy ở ngoài 
//$twtheme->add_ajax('ajax-demo');
$page = new WP_Pages(array(
    'title' => 'Đặt Tour',
    'name' => 'Đặt Tour',
    'slug' => 'booked',
    'icon'=> 'dashicons-phone',
    'order'=> 20
));
