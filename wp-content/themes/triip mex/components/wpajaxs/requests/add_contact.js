jQuery(document).ready(function($) {

    /**
     * home ajax send request
     * @param object TFtheme (created in class.ajax-functions.php)
     */

    /**
     * Ajax loader when scroll to bottom
     * must be definded "action" in class.ajax-functions.php
     */

    $.ajax({
        type: 'POST',
        data: {
            action: 'add_contact',
            nonce: WPAjax.nonce
        },
        url: WPAjax.url,
        beforeSend: function() {

        },
        success: function(data) {
            alert(data);
        }
    });
});