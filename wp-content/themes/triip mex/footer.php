<?php 
$facebook = get_field("link_facebook",'option');
$facebook = empty($facebook)?'https://www.facebook.com/2':$facebook;
$twitter = get_field("link_twitter",'option');
$twitter = empty($twitter)?'https://www.twitter.com/2':$twitter;
$priterest = get_field("link_pinterest",'option');
$pinterest = empty($pinterest)?'https://www.pinterest.com/2':$pinterest;
$instagram = get_field("link_instagram",'option');
$instagram = empty($instagram)?'https://www.instagram.com/2':$instagram;
$copyright = get_field("triip_copyright",'option');
$copyright = empty($copyright)?'2018 Triip Pte. Ltd. All rights reserved.':$copyright;


?>



<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<nav class="footer-nav text-center">
								<ul class="list-inline">
									<li><a href="#">Triip Book</a></li>
									<li><a href="#">Wiki Triip</a></li>
									<li><a href="#">Blog</a></li>
									<li><a href="#">About Us</a></li>
									<li><a href="#">Contact </a></li>
									<li><a href="#">FAQs</a></li>
									<li><a href="#">Policies</a></li>
									<li> <a href="#">Site Map</a></li>
								</ul>
							</nav>
							<div class="footer-contact text-center">
								<ul class="list-inline">
									<li><a class="facebook" href="<?php echo $facebook ?>">"></a></li>
									<li><a class="twitter" href="<?php echo $twitter ?>">"></a></li>
									<li><a class="pinterest" href="<?php echo $pinterest ?>"></a></li>
									<li><a class="instagram" href="<?php echo $instagram ?>"></a></li>
								</ul>
							</div>
							<p class="footer-copyright text-center">&copy; <?php echo $copyright ?></p>
						</div>
					</div>
				</div>
			</footer>
			<?php if (!is_user_logged_in()) { ?>
			<div id="popup-signin">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="group-tabs">
								<!-- Nav tabs-->
								<ul class="nav nav-tabs" role="tablist">
									<li class="active" role="presentation"><a href="#signin" aria-controls="home" role="tab" data-toggle="tab">Singin</a></li>
									<li role="presentation"><a href="#register" aria-controls="profile" role="tab" data-toggle="tab">Register</a></li>
								</ul>
								<!-- Tab panes-->
								<div class="tab-content">
									<div class="tab-pane active" id="signin" role="tabpanel">
										<form id="signin-form" action="" method="post">
											<fieldset>
												<div class="signin-fb">
													<input type="button" value="Sign in with Facebook"/>
												</div>
												<h2 class="line"><span>OR</span></h2>
												<div class="signin-input">
													<label class="icon-input" for="input-email"> <i class="fas fa-envelope-square"></i></label>
													<input id="input-email" type="email" name="email" placeholder="E-mail" required="required"/>
												</div>
												<div class="signin-input">
													<label class="icon-input" for="input-pass"> <i class="fas fa-key"></i></label>
													<input id="input-pass" type="password" min="6" placeholder="Password" name="password" required="required"/>
													<div class="hide-show"><a href="#">Show</a></div>
												</div>
												<div class="signin-input checker">
													<input id="input-checkbox" name="remember" type="checkbox"/>
													<label for="input-checkbox">Remember me </label>
												</div>
												<div class="signin-button text-center">
													<button type="submit" name="submit_singin" value="signin">Sign In</button>
												</div>
												<div class="forgot text-center"><a href="#">Forgot your password?</a></div>
											</fieldset>
										</form>
									</div>
									<div class="tab-pane" id="register" role="tabpanel">
										<form id="register-form" action="" method="post">
											<fieldset>
												<div class="signin-fb">
													<input type="button" value="Sign in with Facebook"/>
												</div>
												<h2 class="line"><span>OR</span></h2>
												<div class="signin-input">
													<label class="icon-input" for="first-name"> <i class="fas fa-user"></i></label>
													<input id="first-name" type="text" name="firstname" placeholder="First Name" required="required"/>
												</div>
												<div class="signin-input">
													<label class="icon-input" for="last-name"> <i class="fas fa-user"></i></label>
													<input id="last-name" type="text" name="lastname" placeholder="Last Name" required="required"/>
												</div>
												<p><span>*</span>Please consider your email is also the username.</p>
												<div class="signin-input">
													<label class="icon-input" for="input-email2"> <i class="fas fa-envelope-square"></i></label>
													<input id="input-email2" type="email" name="email" placeholder="E-mail" required="required"/>
												</div>
												<div class="signin-input checker">
													<input id="tourists" type="radio" name="input-radio" value="not_guide"/>
													<label for="tourists">I'm tourists</label><br/>
													<input id="tour-guide" type="radio" name="input-radio" value="guide"/>
													<label for="tour-guide">I am a tour guide</label>
												</div>
												<div class="signin-input">
													<label class="icon-input" for="input-pass2"> <i class="fas fa-key"></i></label>
													<input id="input-pass2" type="password" min="6" name="password" placeholder="Password" required="required"/>
													<div class="hide-show"><a href="#">Show</a></div>
												</div>
												<div class="accept-terms-paragraph">
													<p>By creating an account you agree to Triip’s <span><a href="#">Terms of Service</a></span></p>
												</div>
												<div class="signin-button text-center">
													<button type="submit" name="submit_register" >Creat-account</button>
												</div>
											</fieldset>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--#footer-->
				</div>
			</div>
			<?php } ?>
		</div>
		<!--#wrapper-->
		<!--JS-->
		<script src="<?php echo TFT_URL ?>/public/libs/jQuery/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="<?php echo TFT_URL ?>/public/libs/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo TFT_URL ?>/public/libs/owlCarousel/owl.carousel.min.js"></script>
		<script src="<?php echo TFT_URL ?>/public/libs/Datepicker/bootstrap-datepicker.min.js"></script>
		<script src="<?php echo TFT_URL ?>/public/libs/typing/typed.min.js"></script>
		<script src="<?php echo TFT_URL ?>/public/libs/selectize/js/standalone/selectize.min.js"></script>
		<script src="<?php echo TFT_URL ?>/public/libs/fancybox-master/jquery.fancybox.min.js"></script>
		<script src="<?php echo TFT_URL ?>/public/libs/validate/additional-methods.min.js"></script>
		<script src="<?php echo TFT_URL ?>/public/libs/validate/jquery.validate.min.js"></script>
		<script src="<?php echo TFT_URL ?>/public/libs/jquery-bar-rating-master/jquery.barrating.min.js"></script>
		<script src="<?php echo TFT_URL ?>/public/js/script.js"></script>
		<?php wp_footer() ?>
	</body>
</html>