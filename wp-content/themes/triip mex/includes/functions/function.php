<?php 


function get_family_terms($taxonomy_name){
	$terms = get_terms( $taxonomy_name, array(
			'hide_empty' => false,
	) );
		$parent=array();
		foreach ($terms as $value) {
			if($value->parent==0){
				$parent[$value->term_id]['name']=$value->name;
				$parent[$value->term_id]['slug']=$value->slug;
			}else{
				$parent[$value->parent]['children'][$value->term_id]=$value;
			}
		}
		return $parent;
}


function set_count_view($post_id){
	$value =get_post_meta( $post_id, 'count_view',true);
	if (empty($value)) {
		add_post_meta( $post_id, 'count_view', 1);
	}else{
		$value++;
		update_post_meta( $post_id, 'count_view', $value );
	}
	return get_post_meta( $post_id, 'count_view',true);
	
}

function count_view($post_id,&$time_watched){
	//$_SESSION['time_visited'];
	$min_minutes = time()-$time_watched;
	if ($min_minutes>200) {
		set_count_view($post_id);
		$time_watched = time();
	}else{
		return ;
	}
}


function add_ranker($post_id,$post_user,$value){
	$rank = get_post_meta( $post_id, 'ranker', false );
	$ranker[$post_user] = $value;
	if(empty($ranker)){
		add_post_meta( $post_id, 'ranker', $ranker, $unique = false );
	}else{
		update_post_meta( $post_id, 'ranker', $ranker );
	}
}


function triip_login($args){
	// $args['user_login'];
	// $args['user_password'];
	// $args['remember'] = true;

	if (!is_user_logged_in()) {
		$user = wp_signon( $args, false );
		if ( is_wp_error($user) ){
		?>
		<script type="text/javascript">
			alert("<?php echo $user->get_error_message() ?>");
		</script>
		<?php
		}
		else{
		?>
		<script type="text/javascript">
			alert("Đăng nhập thành công");
		</script>
		<?php
		}
		
	}
}


function Register($args){

	$new_user = wp_create_user( $args['email'], $args['password'], $args['email']);
	if ( is_wp_error($new_user) ){
		return $new_user->get_error_message();
	}else{
		if ($args['input-radio']=='guide') {
			wp_update_user( array(
				'ID' => $new_user,
				'user_nicename' => $args[ 'name' ],
				'role' => 'author'
			));
		}else{
			wp_update_user( array(
				'ID' => $new_user,
				'user_nicename' => $args[ 'name' ]
			));
		}
		return true;
	}
}


function wp_corenavi_table($custom_query = null) {
	global $wp_query;
	if($custom_query) $main_query = $custom_query;
	else $main_query = $wp_query;
	$big = 999999999;
	$total = isset($main_query->max_num_pages)?$main_query->max_num_pages:'';
	if($total > 1) echo '<div class="paginate_links">';
	echo paginate_links( array(
		 'base'        => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		 'format'   => '?paged=%#%',
		 'current'  => max( 1, get_query_var('paged') ),
		 'total'    => $total,
		 'mid_size' => '10',
		 'prev_text'    => __('Trước','devvn'),
		 'next_text'    => __('Tiếp','devvn'),
	) );
	if($total > 1) echo '</div>';
}


function mt_destination_navigate($permalink_structure = true, $query = null, $link = null) {
	global $wp_query, $wp;
	if($query == null) {
			$query = $wp_query;
	}

	$post_of_page = $query->query_vars['posts_per_page'];
	$total = $query->max_num_pages;
	$current_page = $query->query_vars['paged'];
	if($link==null) {
			$format = ($permalink_structure)?'/page/':'?paged='; 
			$link = get_term_link(get_queried_object(), 'destination').$format;
			$current_page = $query->query_vars['paged'];
	}
	if ( $total > 1 )  { ?>
			<ul class="list-inline">
					<?php 
					if($current_page >1 )
							echo '<li><a href="'.$link.($current_page-1).'"><i class="fas fa-chevron-left"></i></a></li>';
					else
							echo '';
					?>
					<?php for($i=1; $i<=$total; $i++) {
							if($i==$current_page)
									echo '<li><a class="page-active" href="#">'.$i.'</a></li>';
							else
									echo '<li><a href="'.$link.$i.'">'.$i.'</a></li>';
					}
					
					if($current_page < $total)
							echo '<li><a href="'.$link.($current_page+1).'"><i class="fas fa-chevron-right"></i></a></li>';
					else
							echo '';
					?>
			</ul>
	<?php }
}	

function flace_custom_post_type() { 
	$label = array( 
		'name' => __('Các Tour du lịch','twthem'),  
		'singular_name' => __('Các Tour','twthem')
		); 
	$args = array( 
		'labels' => $label,
		'description' => __('Các Tour','twtheme'),
		'supports' => array( 
			'title', 
			'editor', 
			// 'excerpt', 
			// 'author', 
			'thumbnail', 
			'comments', 
			//'trackbacks',//là 
			//'revisions', 
			//'custom-fields' 
		), 
		'taxonomies' => array( 'citys','the-categories' ),
		'hierarchical' => false, 	
		'public' => true, 
		'show_ui' => true, 
		'show_in_menu' => true, 
		'show_in_nav_menus' => true, 
		'show_in_admin_bar' => true, 
		'menu_position' => 25, 
		'menu_icon' => 'dashicons-location',
		'can_export' => true, 
		'has_archive' => true, 
		'exclude_from_search' => false, 
		'publicly_queryable' => true, 
		'capability_type' => 'post' // 
	); 
	register_post_type('the-tours', $args); 
} 

/* Kích hoạt hàm tạo custom post type */ 
add_action('init', 'flace_custom_post_type'); 
function tao_taxonomy() { 
	/* Biến $label chứa các tham số thiết lập tên hiển thị của Taxonomy 
	*/ 
	$labels = array( 
			'name' => 'Các điểm đến', 
			'singular' => 'điểm đến', 
			'menu_name' => 'điểm đến' 
	); 
	$args = array( 
		'labels'			=> $labels, 
		'hierarchical'		=> true,//true giống chuyên mục, false giống thẻ ( tag )
		'public'			=> true,// 
		'show_ui'			=> true, 
		'show_admin_column'	=> true, 
		'show_in_nav_menus'	=> true, 
		'show_tagcloud'		=> true, 
	); 
	/* Hàm register_taxonomy để khởi tạo taxonomy 
	*/ 
	register_taxonomy('places','the-tours', $args); 
} 


function tao_taxonomy_2() { 
	/* Biến $label chứa các tham số thiết lập tên hiển thị của Taxonomy 
	*/ 
	$labels = array( 
			'name' => 'Các chuyên mục', 
			'singular' => 'Chuyên mục', 
			'menu_name' => 'Chuyên mục' 
	); 
	$args = array( 
		'labels'			=> $labels, 
		'hierarchical'		=> true,//true giống chuyên mục, false giống thẻ ( tag )
		'public'			=> true,// 
		'show_ui'			=> true, 
		'show_admin_column'	=> true, 
		'show_in_nav_menus'	=> true, 
		'show_tagcloud'		=> true, 
	); 
	/* Hàm register_taxonomy để khởi tạo taxonomy 
	*/ 
	register_taxonomy('the-categories','the-tours', $args); 
} 
add_action( 'init', 'tao_taxonomy', 0 ); 
add_action( 'init', 'tao_taxonomy_2', 0 ); 

