<?php 

global $wpdb;
$tien_to=$wpdb->prefix;
$params = $_POST['params'];
$limit =3;
$ID=$_POST['ID'];
$params =($params*$limit)-1;
$arr_res=array();

$arr_res['end']=true;

$string='';

$post= get_post( $ID );
$count = $post->comment_count;
if($count < ($params+$limit)){
	$arr_res['end']=false;	
}

$allmiles = $wpdb->prepare( 
	"SELECT * 
		FROM `".$tien_to."comments` 
		WHERE `".$tien_to."comments`.`comment_post_ID`=%d
		ORDER BY `".$tien_to."comments`.`comment_date` 
		DESC LIMIT %d,%d
	",$ID,$params,$limit
);

$res = $wpdb->get_results($allmiles);

foreach ($res as $key => $value) {
	$originalDate = $value->comment_date;
	$newDate = date("d-m-Y", strtotime($originalDate));
	$string='
	<div class="item-comment">
		<img src="'.get_avatar_url( $value->user_id ).'">
		<p class="author">'.$value->comment_author.'</p>
		<p class="date">'.$newDate.'</p>
		<p class="comment">'.$value->comment_content.'</p>
	</div>';
}
$arr_res['string']=$string;
wp_send_json_success($arr_res);
