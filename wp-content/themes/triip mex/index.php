<?php 
/**
 * Index file, the file called if we do not set template for home page
 * @author Trieu Tai Niem
 * @link http://3fgroup.vn
 */

?>

<?php get_header(); ?>

<?php do_action( 'triip_index_page' ) ?>

<?php get_footer(); ?>